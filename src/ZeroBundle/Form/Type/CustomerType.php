<?php
namespace ZeroBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class CustomerType extends AbstractType
{
		public function buildForm(FormBuilderInterface $builder, array $options) {
				$builder
						->add('username', TextType::class, array('label' => 'Логин'))
						->add('password', RepeatedType::class, array(
								'type' => PasswordType::class,
								'first_options' => array('label' => 'Пароль'),
								'second_options' => array('label' => 'Повторите пароль')))
						->add('email', EmailType::class, array('label' => 'Электронная почта'))
						->add('phone', TextType::class, array('label' => 'Телефон'))
						->add('fullname', TextType::class, array('label' => 'Ф.И.О.'))
//						->add('termsAccepted', CheckboxType::class, array(
//								'label' => 'Принимаю условия соглашений',
//								'mapped' => false,
//								'constraints' => new IsTrue(),
//						))
						->add('submit', SubmitType::class, array('label' => 'Отправить'))
						->setMethod('POST');
		}

		public function configureOptions(OptionsResolver $resolver) {
				$resolver->setDefaults(array(
					'data_class' => 'ZeroBundle\Entity\Customer'
				                       ));
		}

}
