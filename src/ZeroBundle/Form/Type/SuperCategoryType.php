<?php
namespace ZeroBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;


class SuperCategoryType extends AbstractType
{
		public function buildForm(FormBuilderInterface $builder, array $options) {
				$builder
						->add('name', TextType::class, array('attr' => array('placeholder' => 'Наименование')))
						->add('alias', TextType::class, array('attr' => array('placeholder' => 'Алиас')))
						->add('description', TextType::class, array('attr' => array('placeholder' => 'Description')))
						->add('save', SubmitType::class, array('label' => 'Сохранить',
						                                       'attr' => array('hover' => '',
						                                                       'ripple-color' => 'tealA400',
						                                                       'bg-red500' => '')));
		}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
				                       'data_class' => 'ZeroBundle\Entity\SuperCategory',
		                       ));
	}
}
