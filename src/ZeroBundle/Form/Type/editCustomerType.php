<?php
namespace ZeroBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class editCustomerType extends AbstractType
{
		public function buildForm(FormBuilderInterface $builder, array $options) {
				$builder
						->add('username', TextType::class, array('label' => 'Логин'))
						->add('password', TextType::class, array('label' => 'Новый пароль'))
						->add('email', EmailType::class, array('label' => 'Электронная почта'))
						->add('phone', TextType::class, array('label' => 'Телефон'))
						->add('fullname', TextType::class, array('label' => 'Ф.И.О.'))
						->add('submit', SubmitType::class, array('label' => 'Отправить'))
						->setMethod('POST');
		}

		public function configureOptions(OptionsResolver $resolver) {
				$resolver->setDefaults(array(
					'data_class' => 'ZeroBundle\Entity\Customer'
				                       ));
		}

}
