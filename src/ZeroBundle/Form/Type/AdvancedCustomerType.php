<?php
namespace ZeroBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class AdvancedCustomerType extends AbstractType
{
		public function buildForm(FormBuilderInterface $builder, array $options) {
				$builder
						->add('userpic', FileType::class, array('label' => 'Аватар',
						                                        'data_class' => null,
						                                        'required' => false))
						->add('oldPassword', PasswordType::class, array('label' => 'Старый пароль',
						                                                'mapped' => false,
						                                                'required' => false))
						->add('newPassword', RepeatedType::class, array(
								'type' => PasswordType::class,
								'first_options' => array('label' => 'Новый пароль',
								                         'required' => false),
								'second_options' => array('label' => 'Повторите новый пароль',
								                          'required' => false),
								'mapped' => false))
						->add('email', EmailType::class, array('label' => 'Электронная почта',
						                                       'required' => false))
						->add('phone', TextType::class, array('label' => 'Телефон',
						                                      'required' => false))
						->add('fullname', TextType::class, array('label' => 'Ф.И.О.',
						                                         'required' => false))
						->add('submit', SubmitType::class, array('label' => 'Отправить'))
						->setMethod('POST');
		}

		public function configureOptions(OptionsResolver $resolver) {
				$resolver->setDefaults(array(
																		'data_class' => 'ZeroBundle\Entity\Customer'
				                            ));
		}

}
