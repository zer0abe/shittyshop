<?php
namespace ZeroBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdditionalFieldType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder
			->add('name', TextType::class, array('attr' => array('placeholder' => 'Ключ')))
			->add('value', TextareaType::class, array('attr' => array('placeholder' => 'Значение')));
			}

	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(array(
				                       'data_class' => 'ZeroBundle\Entity\AdditionalField'
		                       ));
	}
}
