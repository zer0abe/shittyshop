<?php
namespace ZeroBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class DeliveryType extends AbstractType
{
		public function buildForm(FormBuilderInterface $builder, array $options) {
				$builder
						->add('name', TextType::class, array('attr' => array('placeholder' => 'Наименование')))
						->add('price', IntegerType::class, array('label' => 'Стоимость'))
						->add('save', SubmitType::class, array('label' => 'Сохранить'));
		}

	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(array(
				                       'data_class' => 'ZeroBundle\Entity\Delivery'
		                       ));
	}
}
