<?php
namespace ZeroBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class listProductType extends AbstractType
{
		public function buildForm(FormBuilderInterface $builder, array $options) {
				$builder
						->add('name', TextareaType::class, array('label' => 'Наименование'))
						->add('category', EntityType::class, array(
								'class' => 'ZeroBundle:Category',
								'choice_label' => 'name',
								'label' => 'Категория'
						))
						->add('alias', TextType::class, array('label' => 'Алиас'))
						->add('description', TextareaType::class, array('label' => 'Описание'))
						->add('price', TextType::class, array('label' => 'Цена'))
						->add('discount', TextType::class, array('label' => 'Скидка'))
						->add('forSlider', CheckboxType::class, array('label' => 'В слайдер',
//						                                            'expanded' => true,
//						                                            'multiple' => true))
																													))
						->add('save', SubmitType::class, array('label' => 'Сохранить'));
		}
}
