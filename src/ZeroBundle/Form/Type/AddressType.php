<?php
namespace ZeroBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class AddressType extends AbstractType
{
		public function buildForm(FormBuilderInterface $builder, array $options) {
				$builder
						->add('country', TextType::class, array('label' => 'Страна',
						                                        'attr' => array('disabled' => 'disabled'),
						                                        'data' => 'Россия',
						                                        'empty_data' => 'Россия'
						))
						->add('city', ChoiceType::class, array('label' => 'Город склада',
						                                       'choices' => array(
								                                       'Москва' => 'Москва',
								                                       'Орёл' => 'Орёл',
								                                       'Железногорск' => 'Железногорск',
						                                       )))
						->add('post_index', TextType::class, array('label' => 'Индекс',
						                                           'required' => false))
						->add('address', TextType::class, array('label' => 'Желаемый адрес доставки'))
						->add('save', SubmitType::class, array('label' => 'Сохранить'));
		}
}
