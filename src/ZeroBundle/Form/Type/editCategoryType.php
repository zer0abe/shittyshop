<?php
namespace ZeroBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class editCategoryType extends AbstractType
{
		public function buildForm(FormBuilderInterface $builder, array $options) {
				$builder
						->add('id', HiddenType::class)
						->add('name', TextType::class, array('label' => null))
						->add('alias', TextType::class, array('label' => 'Алиас'))
						->add('introText', TextareaType::class, array('label' => 'Вводный текст',
						                                              'attr' => array(
								                                              'class' => 'tinymce',
//								                                              'data-theme' => 'bbcode' // Skip it if you want to use default theme
						                                              )))
						->add('allText', TextareaType::class, array('label' => 'Полный текст',
						                                            'attr' => array(
								                                            'class' => 'tinymce',
//								                                            'data-theme' => 'bbcode' // Skip it if you want to use default theme
						                                            )))
						->add('image', FileType::class, array('label' => 'Изображение',
						                                      'data_class' => null,
						                                      'required' => false))
						->add('description', TextareaType::class, array('label' => 'Описание',
						                                            'attr' => array(
								                                            'class' => 'tinymce',
//								                                            'data-theme' => 'bbcode' // Skip it if you want to use default theme
						                                            )))
						->add('save', SubmitType::class, array('label' => 'Сохранить'))
						->add('edit', SubmitType::class, array('label' => 'Редактировать'));
		}
}
