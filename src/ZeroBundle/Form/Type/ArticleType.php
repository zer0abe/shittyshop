<?php
namespace ZeroBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use ZeroBundle\Form\Type\ImageType;

class ArticleType extends AbstractType
{
		public function buildForm(FormBuilderInterface $builder, array $options) {
				$builder
						->add('name', TextType::class, array('label' => 'Название'))
						->add('category', EntityType::class, array(
								'class' => 'ZeroBundle:ArticleCategory',
								'choice_label' => 'name',
								'label' => 'Категория'))
						->add('content', TextareaType::class, array('label' => 'Наименование',
						                                            'attr' => array('class' => 'tinymce')))
						->add('map', TextareaType::class, array('label' => 'Карта',
						                                        'required' => false))
						->add('images', CollectionType::class, array('entry_type' => ImageType::class,
						                                             'allow_add' => true,
						                                             'allow_delete' => true,
						                                             'prototype' => true,
//						                                             'multiple' => false,
						                                             'entry_options' => array('required' => false,
						                                                                      'data_class' => null,
//						                                                                      'multiple' => false)
																													)))
						->add('save', SubmitType::class, array('label' => 'Сохранить'));
		}

		public function configureOptions(OptionsResolver $resolver) {
			$resolver->setDefaults(array(
				                       'data_class' => 'ZeroBundle\Entity\Article'
		                       ));
		}
}
