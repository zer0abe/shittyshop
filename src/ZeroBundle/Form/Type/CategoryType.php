<?php
namespace ZeroBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;


class CategoryType extends AbstractType
{
		public function buildForm(FormBuilderInterface $builder, array $options) {
				$builder
						->add('supercategory', EntityType::class, array(
								'class' => 'ZeroBundle:SuperCategory',
								'choice_label' => 'name',
								'label' => 'Корневая категория',))
						->add('name', TextType::class, array('attr' => array('placeholder' => 'Наименование')))
						->add('alias', TextType::class, array('attr' => array('placeholder' => 'Алиас')))
						->add('introText', TextareaType::class, array('attr' => array(
								                                              'class' => 'tinymce',
								                                              'placeholder' => 'Вводный текст'
						                                              )))
						->add('allText', TextareaType::class, array('attr' => array(
								                                            'class' => 'tinymce',
								                                            'placeholder' => 'Полный текст'
						                                            )))
						->add('description', TextType::class, array('attr' => array('placeholder' => 'Description')))
//						->add('image', FileType::class, array('label' => 'Изображение',
//						                                      'data_class' => null))
						->add('save', SubmitType::class, array('label' => 'Сохранить',
						                                       'attr' => array('hover' => '',
						                                                       'ripple-color' => 'tealA400',
						                                                       'bg-red500' => '')));
		}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
				                       'data_class' => 'ZeroBundle\Entity\Category',
		                       ));
	}
}
