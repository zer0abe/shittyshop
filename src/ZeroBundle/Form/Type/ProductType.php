<?php
namespace ZeroBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductType extends AbstractType
{
		public function buildForm(FormBuilderInterface $builder, array $options) {
				$builder
						->add('name', TextType::class, array('label' => 'Наименование'))
						->add('category', EntityType::class, array(
								'class' => 'ZeroBundle:Category',
								'choice_label' => 'name',
								'label' => 'Категория'
						))
//						->add('alias', TextType::class, array('label' => 'Алиас'))
						->add('introImg', FileType::class, array('label' => 'Изображение',
						                                         'data_class' => null,
						                                         'required' => false))
						->add('introText', TextareaType::class, array('label' => 'Вводный текст',
						                                              'attr' => array(
								                                              'class' => 'tinymce',
								                                              //								                                              'data-theme' => 'bbcode' // Skip it if you want to use default theme
						                                              )))
						->add('allText', TextareaType::class, array('label' => 'Полный текст',
						                                            'attr' => array(
								                                            'class' => 'tinymce',
						                                            )))
						->add('description', TextType::class, array('label' => 'Описание'))
						->add('price', TextType::class, array('label' => 'Цена'))
						->add('manufacturer', TextType::class, array('label' => 'Производитель',
						                                             'attr' => array('placeholder' => 'Производитель')))
						->add('discount', TextType::class, array('label' => 'Скидка'))
						->add('save', SubmitType::class, array('label' => 'Сохранить и закрыть'))
						->add('additionalFields', CollectionType::class, array('entry_type' => AdditionalFieldType::class,
						                                                    'allow_add' => true,
						                                                    'allow_delete' => true,
						                                                    'prototype' => true,
						                                                    'entry_options' => array(
								                                                    'required' => false,
						                                                        'data_class' => null,
						                                                    )));
		}

		public function configureOptions(OptionsResolver $resolver) {
			$resolver->setDefaults(array(
				                       'data_class' => 'ZeroBundle\Entity\Product'
		                          ));
		}
}
