<?php
namespace ZeroBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FeedbackType extends AbstractType
{
		public function buildForm(FormBuilderInterface $builder, array $options) {
				$builder
						->add('name', TextType::class, array('label' => 'Ф.И.О.'))
						->add('email', EmailType::class, array('label' => 'Ваш почтовый ящик'))
						->add('phone', TextType::class, array('label' => 'Контактный телефон',
						                                      'required' => false))
						->add('content', TextareaType::class, array('label' => 'Ваше сообщение'))
						->add('submit', SubmitType::class, array('label' => 'Отправить'))
						->setMethod('POST');
		}

		public function configureOptions(OptionsResolver $resolver) {
				$resolver->setDefaults(array(
					'data_class' => 'ZeroBundle\Entity\Feedback'
				                       ));
		}

}
