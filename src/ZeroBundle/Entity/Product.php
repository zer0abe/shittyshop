<?php

namespace ZeroBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Product
 */
class Product
{
    /**
     * @var int
     */
    private $id;
    private $promoted;
    private $buddyLike;
    private $unit;
    /**
     * @var
     */
    private $name;

    private $alias;
    /**
     * @var string
     */
    private $introText;

    /**
     * @var string
     */
    private $allText;

    /**
     * @var string
     */
    private $description;

    private $keywords;

    /**
     * @var string
     */
    public $introImg;

    private $category;

    private $manufacturer;

    private $forSlider;

    private $price;

    private $oldPrice;

    private $discount;

    private $calculatedPrice;

    private $createdAt;

    private $clickCounter;

    private $boughtCounter;

    private $additionalFields;

    public function __construct() {
        if(empty($this->boughtCounter))
            $this->boughtCounter = 0;
        if(empty($this->clickCounter))
            $this->clickCounter = 0;
        $this->additionalFields = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function setName($name){
        $this->name = $name;
    }

    public function getName(){
        return $this->name;
    }
    /**
     * Set introText
     *
     * @param string $introText
     *
     * @return Product
     */
    public function setIntroText($introText)
    {
        $this->introText = $introText;

        return $this;
    }

    /**
     * Get introText
     *
     * @return string
     */
    public function getIntroText()
    {
        return $this->introText;
    }

    /**
     * Set allText
     *
     * @param string $allText
     *
     * @return Product
     */
    public function setAllText($allText)
    {
        $this->allText = $allText;

        return $this;
    }

    /**
     * Get allText
     *
     * @return string
     */
    public function getAllText()
    {
        return $this->allText;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Product
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    public function getAbsolutePath(){
        return null === $this->introImg
            ? null
            : $this->getUploadRootDir().'/'.$this->introImg;
    }

    public function getWebPath(){
        return null === $this->introImg
            ? null
            : $this->getUploadDir().'/'.$this->introImg;
    }

    public function getUploadRootDir(){
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    public function getUploadDir(){
        return 'images/Store/Product';
    }

    /**
     * Set introImg
     *
     * @param string $introImg
     *
     * @return Product
     */
    public function setIntroImg($introImg)
    {
        $this->introImg = $introImg;

        return $this;
    }

    /**
     * Get introImg
     *
     * @return string
     */
    public function getIntroImg()
    {
        return $this->introImg;
    }

    /**
     * Set category
     *
     * @param \ZeroBundle\Entity\Category $category
     *
     * @return Product
     */
    public function setCategory(\ZeroBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \ZeroBundle\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }


    public function setCreatedAt() {

    }

    /**
     * @return mixed
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }

    /**
     * Set forSlider
     * @return Product
     * @internal param int $forSlider
     *
     */
    public function setForSlider()
    {
        if($this->forSlider){
            $this->forSlider = false;
        }
        $this->forSlider = true;

    }

    /**
     * Get forSlider
     *
     * @return integer
     */
    public function getForSlider()
    {
        return $this->forSlider;
    }

    /**
     * Set alias
     *
     * @param string $alias
     *
     * @return Product
     */
    public function setAlias($alias = null)
    {
        if($alias === null)
            $alias = $this->getCategory()->getAlias().'_'.$this->getId();
        $this->alias = $alias;

        return $this;
    }

    /**
     * Get alias
     *
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * @return mixed
     */
    public function getPrice() {
        return $this->price;
    }

    /**
     * @param $price
     *
     * @return $this
     */
    public function setPrice($price) {
        $oldprice = $this->price;
        $this->price = $price;
        $this->oldPrice = $oldprice;
        return $this;
    }

    public function getDiscount() {
        return $this->discount;
    }

    public function setDiscount($discount) {
        $this->discount = $discount;

        return $this;
    }

    public function setCalculatedPrice() {
        $this->calculatedPrice = $this->discount
            ? $this->price - ($this->price*($this->discount/100))
            : $this->price;

    }

    public function getCalculatedPrice() {
        return $this->calculatedPrice;
    }

    public function setOldPrice($price) {
        $this->oldPrice = $price;

        return $this;
    }


    /**
     * Get oldPrice
     *
     * @return float
     */
    public function getOldPrice()
    {
        return $this->oldPrice;
    }

    /**
     * @return mixed
     */
    public function getClickCounter() {
        return $this->clickCounter;
    }

    /**
     *
     */
    public function setClickCounter() {
        $this->clickCounter += 1;
    }

    /**
     * @return mixed
     */
    public function getBoughtCounter() {
        $this->boughtCounter +=1;
    }

    /**
     * @param mixed $boughtCounter
     */
    public function setBoughtCounter($boughtCounter) {
        $this->boughtCounter += $boughtCounter;
    }

    /**
     * Set keywords
     *
     * @param string $keywords
     *
     * @return Product
     */
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;

        return $this;
    }

    /**
     * Get keywords
     *
     * @return string
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * @return mixed
     */
    public function getManufacturer() {
        return $this->manufacturer;
    }

    /**
     * @param mixed $manufacturer
     */
    public function setManufacturer($manufacturer) {
        $this->manufacturer = $manufacturer;
    }

    public function getAdditionalFields() {
        return $this->additionalFields;
    }

    public function addAdditionalField(AdditionalField $additionalFields) {
        $this->additionalFields->add($additionalFields);
    }

    public function removeAdditionalField(AdditionalField $additionalField) {
        $this->additionalFields->remove($additionalField);
    }

    /**
     * @return mixed
     */
    public function getPromoted() {
        return $this->promoted;
    }

    /**
     * @param mixed $promoted
     */
    public function setPromoted($promoted) {
        $this->promoted = $promoted;
    }

    /**
     * @return mixed
     */
    public function getBuddyLike() {
        return $this->buddyLike;
    }

    /**
     * @param mixed $buddyLike
     */
    public function setBuddyLike($buddyLike) {
        $this->buddyLike = $buddyLike;
    }

    public function toJson(){
        $array = array();
        foreach($this as $k => $v){
            $array[$k] = $v;
        }

        return json_encode($array);
    }

    /**
     * @return mixed
     */
    public function getUnit() {
        return $this->unit;
    }

    /**
     * @param mixed $unit
     */
    public function setUnit($unit) {
        $this->unit = $unit;
    }
}
