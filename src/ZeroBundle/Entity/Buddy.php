<?php

namespace ZeroBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;


class Buddy
{
		private $id;
		private $name;
		private $options;
		private $allertMessages;
		private $tradeMessages;
		private $jokes;
		private $productsToAdvice;

	/**
	 * Buddy constructor.
	 */
		public function __construct() {
				$this->options = new ArrayCollection();
				$this->allertMessages = new ArrayCollection();
				$this->tradeMessages = new ArrayCollection();
				$this->jokes = new ArrayCollection();
				$this->productsToAdvice = new ArrayCollection();
		}

	/**
	 * @return mixed
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return ArrayCollection
	 */
	public function getAllertMessages() {
		return $this->allertMessages;
	}

	/**
	 * @return ArrayCollection
	 */
	public function getJokes() {
		return $this->jokes;
	}

	/**
	 * @return ArrayCollection
	 */
	public function getOptions() {
		return $this->options;
	}

	/**
	 * @return ArrayCollection
	 */
	public function getProductsToAdvice() {
		return $this->productsToAdvice;
	}

	/**
	 * @return ArrayCollection
	 */
	public function getTradeMessages() {
		return $this->tradeMessages;
	}

	/**
	 * @param $am
	 */
	public function addAllertMessage($am){
		$this->allertMessages->add($am);
	}

	/**
	 * @param $j
	 */
	public function addJoke($j){
		$this->jokes->add($j);
	}

	/**
	 * @param $o
	 */
	public function addOption($o){
		$this->options->add($o);
	}

	/**
	 * @param Product $p
	 */
	public function addProductToAdvice(Product $p){
		$this->productsToAdvice->add($p);
	}

	/**
	 * @param $tm
	 */
	public function addTradeMessage($tm){
		$this->tradeMessages->add($tm);
	}

	/**
	 * @param $am
	 */
	public function removeAllertMessage($am){
		$this->allertMessages->remove($am);
	}

	/**
	 * @param $j
	 */
	public function removeJoke($j){
		$this->jokes->remove($j);
	}

	/**
	 * @param $o
	 */
	public function removeOption($o){
		$this->options->remove($o);
	}

	/**
	 * @param $p
	 */
	public function removeProductToAdvice($p){
		$this->productsToAdvice->remove($p);
	}

	/**
	 * @param $tm
	 *
	 */
	public function removeTradeMessage($tm){
		$this->tradeMessages->remove($tm);
	}

	/**
	 * @return mixed
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @param mixed $name
	 */
	public function setName($name) {
		$this->name = $name;
	}
}