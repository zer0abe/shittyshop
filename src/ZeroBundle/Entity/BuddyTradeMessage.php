<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 09.02.16
 * Time: 15:29
 */

namespace ZeroBundle\Entity;


class BuddyTradeMessage {
		private $id;
		private $buddy;
		private $message;
		private $occasion;

		public function getId() {
				return $this->id;
		}

		public function getBuddy() {
				return $this->buddy;
		}

		public function getOccasion() {
				return $this->occasion;
		}

		public function getMessage() {
				return $this->message;
		}

		public function setBuddy($buddy) {
				$this->buddy = $buddy;
		}

		public function setMessage($message) {
				$this->message = $message;
		}

		public function setId($id) {
				$this->id = $id;
		}

		public function setOccasion($occasion) {
				$this->occasion = $occasion;
		}
}