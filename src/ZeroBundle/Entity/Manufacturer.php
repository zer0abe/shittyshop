<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 24.02.16
 * Time: 13:49
 */

namespace ZeroBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use ZeroBundle\Entity\Product as Product;

class Manufacturer {

		private $id;

		private $name;

		private $products;

		private $description;

		private $legal_entity;

		private $logo;

		private $link;

		public function __construct(){
				$this->products = new ArrayCollection();
		}

		public function getId() {
				return $this->id;
		}

		public function getDescription() {
				return $this->description;
		}

		public function getLegalEntity() {
				return $this->legal_entity;
		}

		public function getProducts() {
				return $this->products;
		}

		public function getName() {
				return $this->name;
		}

		public function getLogo() {
				return $this->logo;
		}

		public function setName($name) {
				$this->name = $name;
		}

		public function setDescription($description) {
				$this->description = $description;
		}

		public function setLegalEntity($legal_entity) {
				$this->legal_entity = $legal_entity;
		}

		public function setLogo($logo) {
				$this->logo = $logo;
		}

		public function addProduct(Product $product){
				$this->products->add($product);
		}

		public function removeProduct(Product $product){
				$this->products->removeElement($product);
		}

	/**
	 * @return mixed
	 */
	public function getLink() {
		return $this->link;
	}

	/**
	 * @param mixed $link
	 */
	public function setLink($link) {
		$this->link = $link;
	}

	public function toJson(){
		$array = array();
		foreach($this as $k => $v){
			$array[$k] = $v;
		}

		return json_encode($array);
	}
}