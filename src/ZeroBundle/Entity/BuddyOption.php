<?php

namespace ZeroBundle\Entity;


class BuddyOption {
		private $id;
		private $key;
		private $value;
		private $buddy;

		public function setBuddy($buddy) {
				$this->buddy = $buddy;
		}

		public function getBuddy() {
				return $this->buddy;
		}

		public function getId() {
				return $this->id;
		}

		public function getKey() {
				return $this->key;
		}

		public function getValue() {
				return $this->value;
		}

		public function setKey($key) {
				$this->key = $key;
		}

		public function setValue($value) {
				$this->value = $value;
		}
}