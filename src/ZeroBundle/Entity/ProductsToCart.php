<?php

namespace ZeroBundle\Entity;

use ZeroBundle\Helper\CartHelper;
use Doctrine\Common\Collections\ArrayCollection;


class ProductsToCart{

		private $id;

		private $cart;

		private $quantity;
    /**
     * @var \ZeroBundle\Entity\Product
     */
    private $product;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     *
     * @return ProductsToCart
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set product
     *
     * @param \ZeroBundle\Entity\Product $product
     *
     * @return ProductsToCart
     */
    public function setProduct(\ZeroBundle\Entity\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \ZeroBundle\Entity\Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set cart
     *
     * @param \ZeroBundle\Entity\Cart $cart
     *
     * @return ProductsToCart
     */
    public function setCart($cart = null)
    {
        $this->cart = $cart;

        return $this;
    }

    /**
     * Get cart
     *
     * @return \ZeroBundle\Entity\Cart
     */
    public function getCart()
    {
        return $this->cart;
    }
}
