<?php

namespace ZeroBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Customer
 */
class Customer implements UserInterface, \Serializable
{
    private $id;

    private $username;

    private $password;

    private $planePassword;

    private $email;

    /**
     * @var string
     */
    private $fullName;

    private $addresses;

    private $cart;

    private $deals;

    private $salt;

    private $userRoles;

    private $phone;

    private $sessionId;

    private $registerDate;

    private $lastvisit;

    /**
     * @var integer
     */
    private $isActive;

    public function __construct(){
        $this->deals = new ArrayCollection();
        $this->addresses = new ArrayCollection();
        $this->userRoles = new ArrayCollection();
        $this->feedbacks = new ArrayCollection();
        $this->registerDate = new \DateTime();
        $this->lastvisit = $this->registerDate;
        $this->salt = md5(uniqid(null, true));
        $this->isActive = true;
        $session = new Session();
        $this->sessionId = $session->getId();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return Customer
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set userpass
     *
     * @param $password
     *
     * @return Customer
     * @internal param string $userpass
     *
     */
//    public function setPassword($password)
//    {
//        $this->password = $password;
//
//        return $this;
//    }

    /**
     * Get userpass
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set salt
     *
     * @param integer $salt
     *
     * @return Customer
     */
//    public function setSalt($salt)
//    {
//        $this->salt = $salt;
//
//        return $this;
//    }

    /**
     * Get salt
     *
     * @return integer
     */
    public function getSalt()
    {
//        return $this->salt;
        return null;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Customer
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set userpic
     *
     * @param string $userpic
     *
     * @return Customer
     */

    /**
     * Get userpic
     *
     * @return string
     */

    /**
     * Set fullName
     *
     * @param string $fullName
     *
     * @return Customer
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;
        return $this;
    }

    /**
     * Get fullName
     *
     * @return string
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Customer
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Customer
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }


    /**
     * Get usergroup
     *
     * @return string
     */
    public function getRoles()
    {
        $roles = array();
        foreach($this->userRoles as $role){
            $roles[] = $role->getName();
        }
        return $roles;
    }

    /**
     * Set sessionId
     *
     * @param string $sessionId
     *
     * @return Customer
     */
    public function setSessionId($sessionId)
    {
        $this->sessionId = $sessionId;

        return $this;
    }

    /**
     * Get sessionId
     *
     * @return string
     */
    public function getSessionId()
    {
        return $this->sessionId;
    }

    /**
     * Get registerDate
     *
     * @return \DateTime
     */
    public function getRegisterDate()
    {
        return $this->registerDate;
    }

    /**
     * Set lastvisit
     *
     * @param \DateTime $lastvisit
     *
     * @return Customer
     */
    public function setLastvisit($lastvisit)
    {
        $this->lastvisit = $lastvisit;

        return $this;
    }

    /**
     * Get lastvisit
     *
     * @return \DateTime
     */
    public function getLastvisit()
    {
        return $this->lastvisit;
    }

    /**
     * Set cart
     *
     * @param \ZeroBundle\Entity\Cart $cart
     *
     * @return Customer
     */
    public function setCart(\ZeroBundle\Entity\Cart $cart = null)
    {
        $this->cart = $cart;

        return $this;
    }

    /**
     * Get cart
     *
     * @return \ZeroBundle\Entity\Cart
     */
    public function getCart()
    {
        if(empty($this->cart)){
            $this->cart = new Cart();
        }
        return $this->cart;
    }

    /**
     * Add deal
     *
     * @param \ZeroBundle\Entity\Deal $deal
     *
     * @return Customer
     */
    public function addDeal(\ZeroBundle\Entity\Deal $deal)
    {
        $this->deals[] = $deal;

        return $this;
    }

    /**
     * Remove deal
     *
     * @param \ZeroBundle\Entity\Deal $deal
     */
    public function removeDeal(\ZeroBundle\Entity\Deal $deal)
    {
        $this->deals->removeElement($deal);
    }

    /**
     * Get deals
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDeals()
    {
        return $this->deals;
    }

    public function equals(UserInterface $customer){
        return md5($this->getUsername()) == md5($customer->getUsername());
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials() {
        // TODO: Implement eraseCredentials() method.
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return Customer
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Add address
     *
     * @param \ZeroBundle\Entity\Address $address
     *
     * @return Customer
     */
    public function addAddress(\ZeroBundle\Entity\Address $address)
    {
        $this->addresses[] = $address;

        return $this;
    }

    /**
     * Remove address
     *
     * @param \ZeroBundle\Entity\Address $address
     */
    public function removeAddress(\ZeroBundle\Entity\Address $address)
    {
        $this->addresses->removeElement($address);
    }

    /**
     * Get addresses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAddresses()
    {
        return $this->addresses;
    }

    /**
     * Add userRole
     *
     * @param \ZeroBundle\Entity\Role $userRole
     *
     * @return Customer
     */
    public function addUserRole(\ZeroBundle\Entity\Role $userRole)
    {
        $this->userRoles[] = $userRole;

        return $this;
    }

    /**
     * Remove userRole
     *
     * @param \ZeroBundle\Entity\Role $userRole
     */
    public function removeUserRole(\ZeroBundle\Entity\Role $userRole)
    {
        $this->userRoles->removeElement($userRole);
    }

    /**
     * Get userRoles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserRoles()
    {
        return $this->userRoles;
    }

    public function serialize()
    {
        return serialize(array(
                             $this->id,
                             $this->username,
                             $this->password,
                             // see section on salt below
                             // $this->salt,
                         ));
    }

    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt
            ) = unserialize($serialized);
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return Customer
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $feedbacks;


    /**
     * Set salt
     *
     * @param integer $salt
     *
     * @return Customer
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Set registerDate
     *
     * @param \DateTime $registerDate
     *
     * @return Customer
     */
    public function setRegisterDate($registerDate)
    {
        $this->registerDate = $registerDate;

        return $this;
    }

    /**
     * Add feedback
     *
     * @param \ZeroBundle\Entity\Feedback $feedback
     *
     * @return Customer
     */
    public function addFeedback(\ZeroBundle\Entity\Feedback $feedback)
    {
        $this->feedbacks[] = $feedback;

        return $this;
    }

    /**
     * Remove feedback
     *
     * @param \ZeroBundle\Entity\Feedback $feedback
     */
    public function removeFeedback(\ZeroBundle\Entity\Feedback $feedback)
    {
        $this->feedbacks->removeElement($feedback);
    }

    /**
     * Get feedbacks
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFeedbacks()
    {
        return $this->feedbacks;
    }

    /**
     * @return mixed
     */
    public function getPlanePassword() {
        return $this->planePassword;
    }

    /**
     * @param mixed $planePassword
     */
    public function setPlanePassword($planePassword) {
        $this->planePassword = $planePassword;
    }
}
