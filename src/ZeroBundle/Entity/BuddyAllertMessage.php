<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 09.02.16
 * Time: 15:24
 */

namespace ZeroBundle\Entity;


class BuddyAllertMessage {
		private $id;
		private $buddy;
		private $message;
		private $occasion;


		public function getId() {
				return $this->id;
		}

		public function getBuddy() {
				return $this->buddy;
		}

		public function getMessage() {
				return $this->message;
		}

		public function setBuddy(Buddy $buddy) {
				$this->buddy = $buddy;
		}

		public function setId($id) {
				$this->id = $id;
		}

		public function setMessage($message) {
				$this->message = $message;
		}

		public function getOccasion() {
				return $this->occasion;
		}

		public function setOccasion($occasion) {
				$this->occasion = $occasion;
		}
}