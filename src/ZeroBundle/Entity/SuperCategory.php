<?php

namespace ZeroBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * SuperCategory
 */
class SuperCategory
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    private $alias;
    /**
     * @var string
     */
    private $description;

    private $categories;

    public function __construct() {
        $this->categories = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return SuperCategory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return SuperCategory
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getCategories() {
        return $this->categories;
    }

    /**
     * @param Category $category
     *
     * @internal param mixed $categories
     */
    public function addCategories(Category $category) {
        $this->categories->add($category);
    }

    public function removeCategories(Category $category){
        $this->categories->remove($category);
    }

    /**
     * @return mixed
     */
    public function getAlias() {
        return $this->alias;
    }

    /**
     * @param mixed $alias
     */
    public function setAlias($alias) {
        $this->alias = $alias;
    }
}

