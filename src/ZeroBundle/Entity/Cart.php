<?php

namespace ZeroBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Bundle\DoctrineBundle\Registry as Doctrine;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\AuthenticationProviderManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;


/**
 * Cart
 */
class Cart implements \Serializable
{
    /**
     * @var int
     */
    private $id;

    private $products;

    private $customer;

    private $counter;

    public function __construct(){
        $this->products = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set Customer
     *
     * @param \ZeroBundle\Entity\Customer $customer
     *
     * @return $this
     */
    public function setCustomer(\ZeroBundle\Entity\Customer $customer){
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get Customer
     *
     * @return mixed
     */
    public function getCustomer(){
        return $this->customer;
    }

    public function addProduct(\ZeroBundle\Entity\Product $product) {
        $this->products = $product;
        return $this;
    }

    public function removeProduct(\ZeroBundle\Entity\Product $product){
        $this->products->removeElement($product);
    }

    public function getProducts() {
        return $this->products;
    }

    public function serialize()
    {
        return serialize(array(
                             $this->id,
                             $this->customer,
                             $this->products
                         ));
    }

    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->customer,
            $this->products
            ) = unserialize($serialized);
    }

    public function setCounter(){
        $counter = 0;
        $pLength = count($this->products);
        for($i = 0; $i < $pLength; $i++) {
            $counter += 1;
        }
        $this->counter = $counter;
        return $this;
    }

    public function getCounter(){
        $counter = 0;
        $pLength = count($this->products);
        echo '<br> '.$pLength;
        if($pLength > 0) {
            for ($i = 0; $i < $pLength; $i++) {
                $counter += 1;
            }
        }
        $this->counter = $counter;
        return $this->counter;
    }

}
