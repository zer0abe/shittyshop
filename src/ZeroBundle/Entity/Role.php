<?php
namespace ZeroBundle\Entity;

use Symfony\Component\Security\Core\Role\RoleInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="role")
 */

class Role implements RoleInterface
{
		/**
		 * @ORM\Id
		 * @ORM\Column(type="integer")
		 * @ORM\GeneratedValue(strategy="AUTO")
		 *
		 * @vat integer id
		 */
		protected $id;

		/**
		 * @ORM\Column(type="string", length="255")
		 *
		 * @var string name
		 */
		protected $name;

		/**
		 * @ORM\Column(type="DateTime", name="created_at")
		 *
		 * @var DateTime $createdAt
		 */
		protected $createdAt;

		public function getId() {
				return $this->id;
		}

		public function getName() {
				return $this->name;
		}

		public function setName($name) {
				$this->name = $name;
		}

		public function getCreatedAt() {
				return $this->createdAt;
		}

		public function __construct(){
				$this->createdAt = new \DateTime();
		}


		/**
		 * @return mixed
		 */
		public function getRole() {

				return $this->getName();
		}

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Role
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
