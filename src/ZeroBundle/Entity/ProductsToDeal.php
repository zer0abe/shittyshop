<?php

namespace ZeroBundle\Entity;

/**
 * ProductsToDeal
 */
class ProductsToDeal
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $quantity;

    /**
     * @var float
     */
    private $costs;

    private $product;

    private $deal;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     *
     * @return ProductsToDeal
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set costs
     * @return ProductsToDeal
     * @internal param float $costs
     *
     */
    public function setCosts()
    {
        $this->costs = $this->getProduct()->getCalculatedPrice() * $this->getQuantity();

        return $this;
    }

    /**
     * Get costs
     *
     * @return float
     */
    public function getCosts()
    {
        return $this->costs;
    }

    /**
     * @return mixed
     */
    public function getProduct() {
        return $this->product;

    }

    /**
     * @param mixed $product
     *
     * @return $this
     */
    public function setProduct($product) {
        $this->product = $product;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDeal() {
        return $this->deal;
    }

    /**
     * @param mixed $deal
     *
     * @return $this
     */
    public function setDeal($deal) {
        $this->deal = $deal;

        return $this;
    }
}
