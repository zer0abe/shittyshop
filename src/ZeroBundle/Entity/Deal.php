<?php

namespace ZeroBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
/**
 * Deal
 */
class Deal
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var \ZeroBundle\Entity\Customer
     */
    private $customer;

    private $delivery;

    /**
     * @var float
     */
    private $costs;

    /**
     * @var string
     */
    private $deliveryAddress;

    /**
     * @var \DateTime
     */
    private $submitDate;

    /**
     * @var int
     */
    private $status;

    private $productsToDeal;

    public function __construct(){

        $this->productsToDeal = new ArrayCollection();

    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set costs
     *
     * @param float $costs
     *
     * @return Deal
     */
    public function setCosts($costs)
    {
        $this->costs = $costs;

        return $this;
    }

    /**
     * Get costs
     *
     * @return float
     */
    public function getCosts()
    {
        return $this->costs;
    }

    /**
     * Set deliveryAddress
     *
     * @param string $deliveryAddress
     *
     * @return Deal
     */
    public function setDeliveryAddress($deliveryAddress)
    {
        $this->deliveryAddress = $deliveryAddress;

        return $this;
    }

    /**
     * Get deliveryAddress
     *
     * @return string
     */
    public function getDeliveryAddress()
    {
        return $this->deliveryAddress;
    }

    /**
     * Set submitDate
     *
     * @param \DateTime $submitDate
     *
     * @return Deal
     */
    public function setSubmitDate($submitDate)
    {
        $this->submitDate = $submitDate;

        return $this;
    }

    /**
     * Get submitDate
     *
     * @return \DateTime
     */
    public function getSubmitDate()
    {
        return $this->submitDate;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Deal
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set customer
     *
     * @param \ZeroBundle\Entity\Customer $customer
     *
     * @return Deal
     */
    public function setCustomer(\ZeroBundle\Entity\Customer $customer = null)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return \ZeroBundle\Entity\Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Add product
     *
     * @param \ZeroBundle\Entity\Product $product
     *
     * @return Deal
     */
    public function addProductToDeal(\ZeroBundle\Entity\ProductsToDeal $product)
    {
        $this->productsToDeal[] = $product;
//        $this->setCosts($product->getCosts());

        return $this;
    }

    /**
     * Remove product
     *
     * @param \ZeroBundle\Entity\Product $product
     */
    public function removeProduct(\ZeroBundle\Entity\ProductsToDeal $product)
    {
        $this->productsToDeal->removeElement($product);
    }

    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProducts()
    {
        return $this->productsToDeal;
    }

    /**
     * Add productsToDeal
     *
     * @param \ZeroBundle\Entity\ProductsToDeal $productsToDeal
     *
     * @return Deal
     */
    public function addProductsToDeal(\ZeroBundle\Entity\ProductsToDeal $productsToDeal)
    {
        $this->productsToDeal[] = $productsToDeal;

        return $this;
    }

    /**
     * Remove productsToDeal
     *
     * @param \ZeroBundle\Entity\ProductsToDeal $productsToDeal
     */
    public function removeProductsToDeal(\ZeroBundle\Entity\ProductsToDeal $productsToDeal)
    {
        $this->productsToDeal->removeElement($productsToDeal);
    }

    /**
     * Get productsToDeal
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductsToDeal()
    {
        return $this->productsToDeal;
    }

    /**
     * @return mixed
     */
    public function getDelivery() {
        return $this->delivery;
    }

    /**
     * @param mixed $delivery
     */
    public function setDelivery(Delivery $delivery) {
        $this->delivery = $delivery;
    }
}
