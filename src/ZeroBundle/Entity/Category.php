<?php

namespace ZeroBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use ZeroBundle\Helpers\GlobalsHelper as Helper;


/**
 * Category
 */
class Category
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $introText;

    /**
     * @var string
     */
    private $allText;

    /**
     * @var string
     */
    private $image;

    private $children;

    private $parent;

    /**
     * @var string
     */
    private $description;

    private $keywords;

    private $supercategory;

    /**
     * @var string
     */
    private $name;

    private $products;

    public function __construct($id = null){
        $this->products = new ArrayCollection();
        $this->children = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set introText
     *
     * @param string $introText
     *
     * @return Category
     */
    public function setIntroText($introText)
    {
        $this->introText = $introText;

        return $this;
    }

    /**
     * Get introText
     *
     * @return string
     */
    public function getIntroText()
    {
        return $this->introText;
    }

    /**
     * Set allText
     *
     * @param string $allText
     *
     * @return Category
     */
    public function setAllText($allText)
    {
        $this->allText = $allText;

        return $this;
    }

    /**
     * Get allText
     *
     * @return string
     */
    public function getAllText()
    {
        return $this->allText;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Category
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Category
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Add product
     *
     * @param \ZeroBundle\Entity\Product $product
     *
     * @return Category
     */
    public function addProduct(\ZeroBundle\Entity\Product $product)
    {
        $this->products[] = $product;

        return $this;
    }

    /**
     * Remove product
     *
     * @param \ZeroBundle\Entity\Product $product
     */
    public function removeProduct(\ZeroBundle\Entity\Product $product)
    {
        $this->products->removeElement($product);
    }

    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @var string
     */
    private $alias;


    /**
     * Set alias
     *
     * @param string $alias
     *
     * @return Category
     */
    public function setAlias($alias = null)
    {
        if(empty($alias))
            $alias = Helper::transliterate($this->name);
        $this->alias = $alias;

        return $this;
    }

    /**
     * Get alias
     *
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * @return mixed
     */
    public function getKeywords() {
        return $this->keywords;
    }

    /**
     * @param mixed $keywords
     */
    public function setKeywords($keywords) {
        $this->keywords = $keywords;
    }
    public function addChildren(\ZeroBundle\Entity\Category $category)
    {
        $this->children[] = $category;

        return $this;
    }


    public function removeChildren(\ZeroBundle\Entity\Category $category)
    {
        $this->products->removeElement($category);
    }

    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @return mixed
     */
    public function getParent() {
        return $this->parent;
    }

    /**
     * @param mixed $parent
     */
    public function setParent(Category $parent = null) {
        $this->parent = $parent;
        $parent->addChildren($this);
    }

    /**
     * @return mixed
     */
    public function getSupercategory() {
        return $this->supercategory;
    }

    /**
     * @param mixed $supercat
     */
    public function setSupercategory($supercat) {
        $this->supercategory = $supercat;
    }

    public function toJson(){
        $array = array();
        foreach($this as $k => $v){
            $array[$k] = $v;
        }

        return json_encode($array);
    }
}
