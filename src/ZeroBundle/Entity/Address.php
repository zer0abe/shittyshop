<?php

namespace ZeroBundle\Entity;

/**
 * Address
 */
class Address
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $country;

    /**
     * @var string
     */
    private $city;

    /**
     * @var string
     */
    private $postIndex;

    /**
     * @var string
     */
    private $address;

    /**
     * @var \ZeroBundle\Entity\Customer
     */
    private $customer;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set country
     *
     * @param string $country
     *
     * @return Address
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Address
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set postIndex
     *
     * @param string $postIndex
     *
     * @return Address
     */
    public function setPostIndex($postIndex)
    {
        $this->postIndex = $postIndex;

        return $this;
    }

    /**
     * Get postIndex
     *
     * @return string
     */
    public function getPostIndex()
    {
        return $this->postIndex;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Address
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set customer
     *
     * @param \ZeroBundle\Entity\Customer $customer
     *
     * @return Address
     */
    public function setCustomer(\ZeroBundle\Entity\Customer $customer = null)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return \ZeroBundle\Entity\Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    public function __toString() {
        // TODO: Implement __toString() method.
        $address = $this->postIndex.', '.$this->country.', '.$this->city.', '.$this->address;
        return $address;
    }
}
