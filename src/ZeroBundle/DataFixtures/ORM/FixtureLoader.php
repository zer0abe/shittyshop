<?php

namespace ZeroBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use ZeroBundle\Entity\Address;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use ZeroBundle\Entity\Role;
use ZeroBundle\Entity\Customer;

class FixtureLoader implements FixtureInterface, ContainerAwareInterface
{

		private $container;
		/**
		 * Load data fixtures with the passed EntityManager
		 *
		 * @param ObjectManager $manager
		 */
		public function load(ObjectManager $manager) {
			$encoder = $this->container->get('security.password_encoder');
			$userRole1 = new Role();
			$userRole1->setName('ROLE_ADMIN');
			$userRole2 = new Role();
			$userRole2->setName('ROLE_USER');
			$addres = new Address();
			$addres->setCity('Железногорск');
			$addres->setCountry('Россия');
			$addres->setPostIndex('307170');
			$addres->setAddress('ул. Курская, д. 4');

			$user = new Customer();
			$user->setEmail('zeroabe@ya.ru');
			$user->setUsername('zeroabe');
			$user->setPlanePassword('raco0n321');
			$user->addUserRole($userRole1);
			$user->addUserRole($userRole2);
			$user->setPassword($encoder->encodePassword($user, 'raco0n321'));
			$user->setAddress($addres);
			$addres->setCustomer($user);
			$manager->persist($userRole1);
			$manager->persist($userRole2);
			$manager->persist($user);
			$manager->persist($addres);
			$manager->flush();
		}

	public function setContainer(ContainerInterface $container = null)
	{
		$this->container = $container;
	}
}
