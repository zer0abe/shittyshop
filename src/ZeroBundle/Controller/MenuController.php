<?php

namespace ZeroBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MenuController extends Controller
{
    public function topMenuAction()
    {
        $em = $this->getDoctrine()->getManager();
        $catalog = array();
        $sCat = $em->getRepository('ZeroBundle:SuperCategory')->findAll();
        foreach($sCat as $item){
            $catalog[] = $item;
        }
        $articleCategory = $em->getRepository('ZeroBundle:ArticleCategory')->findAll();
        return $this->render('ZeroBundle:Menu:top_menu.html.twig', array('articles' => $articleCategory));
    }

    public function leftMenuAction()
    {
        $em = $this->getDoctrine()->getManager();
        $super = $em->getRepository('ZeroBundle:SuperCategory')->findAll();
        return $this->render('ZeroBundle:Menu:left_menu.html.twig', array('super' => $super));
    }
}
