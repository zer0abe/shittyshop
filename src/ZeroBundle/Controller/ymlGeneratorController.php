<?php

namespace ZeroBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ymlGeneratorController extends Controller
{
    public function generateAction()
    {
        $categories = $this->getDoctrine()->getRepository('ZeroBundle:Category')->findAll();
        $products = $this->getDoctrine()->getRepository('ZeroBundle:Product')->findAll();
        $curDate = new \DateTime();
        return $this->render('ZeroBundle:ymlGenerator:proffi_catalog.xml.twig',
                             array('categories' => $categories,
                                   'products' => $products,
                                   'date' => $curDate));
    }

}
