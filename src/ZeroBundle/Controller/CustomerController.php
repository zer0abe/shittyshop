<?php

namespace ZeroBundle\Controller;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use ZeroBundle\Entity\Address;
use ZeroBundle\Entity\Customer;
use ZeroBundle\Entity\Deal;
use ZeroBundle\Entity\ProductsToDeal;
use ZeroBundle\Form\Type\CustomerType;
use ZeroBundle\Form\Type\AddressType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;


class CustomerController extends Controller
{

    public function cartConfirmationAction(Request $request){
        $session = new Session();

        $em = $this->getDoctrine()->getManager();

        if(!$customer = $this->getUser()) {
            $referer = $request->headers->get('referer');
            $session->set('referer', $referer);
            return $this->redirectToRoute('login_path');
        }else{
            if($cart = $em->getRepository('ZeroBundle:Cart')->find($session->get('cart'))) {
                if ($anotherCart = $customer->getCart()) {
                    if($anotherCart->getId() != $cart->getId()) {
                        foreach ($anotherCart->getProducts() as $product) {
                            $em->remove($product);
                            $em->flush();
                        }
                        $customer->setCart(null);
                        $em->remove($anotherCart);
                        $em->persist($customer);
                        $em->flush();
                    }
                }
            }
            $customer->setCart($cart);
            $cart->setCustomer($customer);
            $em->persist($customer);
            $em->persist($cart);
            $em->flush();
            //TODO У нас есть авторизованый пользователь и у него есть корзина.

            $ptc = $cart->getProducts();
            $costs = 0;     //Подсчитываем общую стоимость покупки
            foreach($ptc as $p){
                $costs += ($p->getQuantity() * $p->getProduct()->getCalculatedPrice());
            }
            $deal = new Deal();
            $addresses = $customer->getAddresses();
            $choiceAddr = array();
            foreach($addresses as $k => $v){
                $choiceAddr[] = $v;
            }
            if(count($choiceAddr) > 0) {
//                $form = $this->createFormBuilder()
//                    ->add(
//                        'address', ChoiceType::class, array('choices' => $choiceAddr,
//                                                            'choice_label' =>
//                                                                function ($value, $key, $index)
//                                                                {
//                                                                    return $value;
//                                                                },
//                                                            'choices_as_values' => true,
//                                                            'label' => 'Адрес доставки'
//                        ))
//                    ->add('delivery', EntityType::class, array('class' => 'ZeroBundle:Delivery',
//                                                               'choice_label' => 'name',
//                                                               'expanded' => true,
//                                                               'multiple' => false))
//                    ->add('save', SubmitType::class, array('label' => 'Подтвердить'))
//                    ->getForm();
//                $form->handleRequest($request);
                $deliveries = $em->getRepository('ZeroBundle:Delivery')->findAll();
                if($form = $request->request->get('form')){
                    $deal->setCustomer($customer);
                    if(isset($form['address']))
                    $deal->setDeliveryAddress($form['address']);
                    $deal->setSubmitDate(new \DateTime());
                    $deal->setCosts($costs);
                    $delivery = $em->getRepository('ZeroBundle:Delivery')->find($form['delivery']);
                    $deal->setDelivery($delivery);
                    foreach($ptc as $product){
                        $ptd = new ProductsToDeal();
                        $ptd->setProduct($product->getProduct());
                        $ptd->setQuantity($product->getQuantity());
                        $ptd->setCosts();
                        $ptd->setDeal($deal);
                        $deal->addProductToDeal($ptd);
                        $em->persist($ptd);
                        $em->persist($deal);
                        $em->flush();
                    }

                    $em->persist($deal);

                    foreach($ptc as $p){
                        $em->remove($p);
                    }

                    $em->remove($cart);
                    $customer->setCart(null);
                    $em->persist($cart);
                    $em->persist($customer);
                    $em->flush();
                    $session->remove('cart');
                    $message = '<p>Ваша покупка оформлена. В ближайшее время с Вами свяжется наш Менеджер.</p>';
                    $message .='<p>Пожалуйста, убедитесь в том, что Вы ввели правильные контактные данные при
                    регистрации.</p>';
                    $url = $this->get('router')->generate('_customer_edit');
                    $message .= '<p>Сделать это можно, перейдя по этой <a href="'.$url.'">ссылке</a></p>';
                    $this->addFlash('notice', $message);
                    $email = \Swift_Message::newInstance()
                        ->setSubject('Служба поддержки пользователей smesiproffi.ru')
                        ->setFrom('feedback@smesiproffi.ru')
                        ->setTo($customer->getEmail())
                        ->setBody($this->renderView('Emails/deal.html.twig',
                                                    array('deal' => $deal),
                                                    'text/html'));
                    $this->get('mailer')->send($email);
                    return $this->render('ZeroBundle:Customer:deal.html.twig',
                                         array('id' => $deal->getId(),
                                               'deal' => $deal,
                                               'customer' => $customer,
                                               'products' => $deal->getProducts()));
                }

                return $this->render('ZeroBundle:Default:makeADeal_step1.html.twig',
                                     array('addresses' => $addresses,
                                           'deliveries' => $deliveries));
            }else{
                $referer = $request->headers->get('referer');
                $session->set('referer', $referer);
                return $this->redirectToRoute('_new_address');
            }

        }

    }

    public function registrationAction(Request $request)
    {
        $session = new Session();
        $customer = new Customer();
        $form = $this->createForm(CustomerType::class, $customer, array(
            'action' => $this->generateUrl('_registration')
        ));

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();

            $customer->setPassword($this->get('security.password_encoder')
            ->encodePassword($customer, $form->get('password')->getData()));
            $role = $this->getDoctrine()->getRepository('ZeroBundle:Role')
                ->findOneBy(array('name' => 'ROLE_USER'));
            $customer->addUserRole($role);

            $customer->setPlanePassword($form->get('password')->getData());

            if($session->has('cart')) {
                $cart = $this->getDoctrine()->getRepository('ZeroBundle:Cart')
                    ->find($session->get('cart'));
                $customer->setCart($cart);
                $cart->setCustomer($customer);
                $em->persist($cart);
            }


            $em->persist($customer);
            $em->flush();
            $token = new UsernamePasswordToken($customer, null, 'main', $customer->getRoles());
            $this->get('security.token_storage')->setToken($token);

            if($session->has('referer')){
                $url = $session->get('referer');
                $session->remove('referer');
                return $this->redirect($url);
            }
            return $this->redirectToRoute('_homepage');
        }
        return $this->render('ZeroBundle:Customer:registration.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function newAddressAction(Request $request){
        $address = new Address();
        $customer = $this->getUser();
        $form = $this->createForm(AddressType::class, $address);

        $form->handleRequest($request);

        if($form->isValid() && $form->isSubmitted()){
            $session = new Session();
            $em = $this->getDoctrine()->getManager();
            $customer->addAddress($address);
            $address->setCustomer($customer);
            $em->persist($address);
            $em->persist($customer);
            $em->flush();
            if($session->has('referer')){
                $referer = $session->get('referer');
                $session->remove('referer');
                return $this->redirect($referer);
            }
        }

        return $this->render('ZeroBundle:Customer:newAddress.html.twig', array(
            'form' => $form->createView(),
            'customer' => $customer
        ));
    }

    public function showDealsAction(){
        $customer = $this->getUser();
        $deals = $customer->getDeals();

        return $this->render('ZeroBundle:Customer:deals.html.twig', array('deals' => $deals,
                                                                          'customer' => $customer));

    }

    public function showDealAction($id){
//        $em = $this->getDoctrine()->getManager();
        $customer = $this->getUser();
        $deal = $this->getDoctrine()->getRepository('ZeroBundle:Deal')->find($id);
        $products = $deal->getProducts();
        return $this->render('ZeroBundle:Customer:deal.html.twig', array('deal' => $deal,
                                                                                'products' => $products,
                                                                                'customer' => $customer,
                                                                                'message' => null));
    }

    public function cabinetAction(){

        $customer = $this->getUser();

        return $this->render('ZeroBundle:Customer:cabinet.html.twig',
                             array('customer' => $customer));
    }

    public function editAction(Request $request){
        $session = new Session();
        $customer = $this->getUser();
        $em = $this->getDoctrine()->getManager();
//        $messages = array();
//        $errors = array();
        $form = $this->createFormBuilder()

            ->add('oldPassword', PasswordType::class, array('label' => 'Старый пароль',
                                                            'mapped' => false,
                                                            'required' => false))
            ->add('newPassword', RepeatedType::class, array(
                'type' => PasswordType::class,
                'first_options' => array('label' => 'Новый пароль',
                                         'required' => false),
                'second_options' => array('label' => 'Повторите новый пароль',
                                          'required' => false),
                'mapped' => false))
            ->add('email', EmailType::class, array('label' => 'Электронная почта',
                                                   'required' => false,
                                                   'data' => $customer->getEmail()))
            ->add('phone', TextType::class, array('label' => 'Телефон',
                                                  'required' => false,
                                                  'data' => $customer->getPhone()))
            ->add('fullname', TextType::class, array('label' => 'Ф.И.О.',
                                                     'required' => false,
                                                     'data' => $customer->getFullName()))
            ->add('submit', SubmitType::class, array('label' => 'Отправить'))
            ->getForm();

        //Обрабатываем запрос на предмет содержания
        // подтверждённой формы
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            //если форма содержит Изображение пользователя
//            if($form->get('userpic')->getData()){
//                $image = $form->get('userpic')->getData();
//                $imageName = $customer->getUsername();
//                $image->move('images/Store/Customer/', $imageName);
//                $customer->setUserpic($imageName);
//                $session->getFlashBag()->add('notice','Вы изменили свой Аватар.');
//            }else{
//                $image = $customer->getUserPic();
//                $customer->setUserPic($image);
//            }
            //если форма содержит новый пароль
            if($form->get('newPassword')->getData()){
                $storredPass = $customer->getPlanePassword();
                $deliveredPass = $form->get('oldPassword')->getData();
                if($storredPass == $deliveredPass){
                    $customer->setPassword($this->get('security.password_encoder')
                                               ->encodePassword($customer,
                                                                $form->get('newPassword')
                                                                    ->getData()));
                    $customer->setPlanePassword($form->get('newPassword')->getData());
                    $session->getFlashBag()->add('notice','Вы успешно изменили свой старый пароль');
                    $message = \Swift_Message::newInstance()
                        ->setSubject('Служба поддержки пользователей smesiproffi.ru')
                        ->setFrom('feedback@smesiproffi.ru')
                        ->setTo($customer->getEmail())
                        ->setBody(
                            $this->renderView(
                                'Emails/customerChange.html.twig',
                                array('customer' => $customer)
                            ),
                            'text/html'
                        );
                    $this->get('mailer')->send($message);
                }else $session->getFlashBag()->add('error','Вы допустили ошибку в старом пароле');
            }
            //если форма содержит новый почтовый ящик
            if($form->get('email')->getData()){
                $customer->setEmail($form->get('email')->getData());
                $session->getFlashBag()->add('notice','Вы успешно сменили свой почтовый ящик');
            }

            //если форма содержит новый телефон
            if($form->get('phone')->getData()){
                $customer->setPhone($form->get('phone')->getData());
                $session->getFlashBag()->add('notice','Вы успешно изменили свой контактный телефон.');
            }

            //Если форма содержит новые Ф.И.О.
            if($form->get('fullname')->getData()){
                $customer->setFullName($form->get('fullname')->getData());
                $session->getFlashBag()->add('notice','Вы изменили полное имя.');
            }

            $em->persist($customer);
            $em->flush();
            return $this->render('ZeroBundle:Customer:afterEdit.html.twig', array('customer' => $customer));
        }

        return $this->render('ZeroBundle:Customer:edit.html.twig',
                             array('form' => $form->createView(),
                                   'customer' => $customer));

    }
}

