<?php

namespace ZeroBundle\Controller;

use Doctrine\ORM\NoResultException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ZeroBundle\Entity\Delivery;
use ZeroBundle\Entity\Manufacturer;
use ZeroBundle\Entity\SuperCategory;
use ZeroBundle\Form\Type\CategoryType;
use ZeroBundle\Form\Type\ProductType;
use ZeroBundle\Form\Type\listCustomerType;
use ZeroBundle\Form\Type\editCustomerType;
use ZeroBundle\Form\Type\SuperCategoryType;
use ZeroBundle\Form\Type\DeliveryType;
use Symfony\Component\HttpFoundation\Request;
use ZeroBundle\Entity\Product;
use ZeroBundle\Entity\Category;
use ZeroBundle\Entity\Customer;
use ZeroBundle\Entity\Deal;
use ZeroBundle\Entity\Feedback;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class AdminController extends Controller
{
    public function indexAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $paginator = $this->get('knp_paginator');
        $dql = "SELECT p FROM ZeroBundle:Product p";
        $query = $em->createQuery($dql);
        $pagination = $paginator->paginate($query,
                                           $request->query->getInt('page', 1),
                                           $request->query->getInt('limit', 5));
        return $this->render('ZeroBundle:Admin:index.html.twig', array('pagination' => $pagination));
    }


    public function listCategoriesAction(Request $request = null){
        $em = $this->getDoctrine()->getManager();

        $query = $em->createQueryBuilder()
            ->select('c')
            ->from('ZeroBundle:SuperCategory', 'c');

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate($query,
                                           $request->query->getInt('page', 1),
                                           5);
//        print_r($pagination);
        return $this->render('ZeroBundle:Admin:listCategories.html.twig',
                             array('pagination' => $pagination));
    }

    public function listProductsAction(Request $request, $id = null){
        $em = $this->getDoctrine()->getManager();
        if(null !== $id){
            $category = $em->getRepository('ZeroBundle:Category')->find($id);
            $products = $category->getProducts();
        }else
            $products = $this->getDoctrine()->getRepository('ZeroBundle:Product')->findAll();
            $category = null;

        return $this->render('ZeroBundle:Admin:listProducts.html.twig',
                             array('products' => $products,
                                   'category' => $category));
    }

    public function listCategoryProductsAction(Request $request, $id = null){
        $em = $this->getDoctrine()->getManager();
            $category = $em->getRepository('ZeroBundle:Category')->find($id);
            $products = $category->getProducts();

        return $this->render('ZeroBundle:Admin:listCategoryProducts.html.twig',
                             array('products' => $products,
                                   'category' => $category));
    }

    public function listCustomersAction(){
        $customers = $this->getDoctrine()->getRepository('ZeroBundle:Customer')->findAll();
        $forms = array();
        foreach($customers as $customer){
            $form = $this->createForm(listCustomerType::class, $customer)->createView();
            $forms[] = $form;
        }

        return $this->render('ZeroBundle:Admin:listCustomers.html.twig', array('forms' => $forms));
    }

    public function listDealsAction(Request $request){
        $em = $this->getDoctrine()->getManager();

        $query = $em->createQueryBuilder()
            ->select('d')
            ->from('ZeroBundle:Deal', 'd');

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate($query,
                                            $request->query->getInt('page', 1),
                                            25);

        return $this->render('ZeroBundle:Admin:listDeals.html.twig', array('pagination' => $pagination));
    }

    public function listFeedBackAction(Request $request){
        $em = $this->getDoctrine()->getManager();

        $query = $em->createQueryBuilder()
            ->select('f')
            ->from('ZeroBundle:Feedback', 'f');

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate($query,
                                           $request->query->getInt('page', 1),
                                           10);

        return $this->render('ZeroBundle:Admin:listFeedbacks.html.twig',
                             array('pagination' => $pagination));
    }

    public function editCategoryAction(Request $request, $id = null){
        $em = $this->getDoctrine()->getManager();
        if(null !== $id)
            $category = $em->getRepository('ZeroBundle:Category')->find($id);
        else
            $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $em->persist($category);
            $em->flush();

            return $this->redirectToRoute('list_categories');
        }

        return $this->render('ZeroBundle:Admin:newCategory.html.twig',
                             array('form' => $form->createView(),
                                   'category' => $category
                                   ));
    }


    public function editProductAction(Request $request, $id = null, $alias = null){
        $em = $this->getDoctrine()->getManager();
        if(null !== $id)
            $product = $em->getRepository('ZeroBundle:Product')->find($id);
        else if(null !== $alias ){
            $category = $em->getRepository('ZeroBundle:Category')->findOneBy(array('alias'=>$alias));
            $product = new Product();
            $product->setCategory($category);
        }else
            $product = new Product();
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $product->setAlias();
            if($image = $form->get('introImg')->getData()) {
                $imageName = md5(uniqid()).'.'. $image->getClientOriginalExtension();
                $image->move('images/Store/Product/', $imageName);
                $product->setIntroImg($imageName);
            }else {
                $image = $product->getIntroImg();
                $product->setIntroImg($image);
            }
            $product->setCalculatedPrice();
            $em->persist($product);
            $em->flush();
            $product = $this->getDoctrine()->getRepository('ZeroBundle:Product')
                ->find($product->getId());
            $product->setAlias();
            $em->persist($product);
            $em->flush();
            return $this->redirectToRoute('list_products_in_category',
                                          array('id' => $product->getCategory()->getId()));
        }

        return $this->render('ZeroBundle:Admin:newProduct.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function searchCustomerAction(Request $request, $customer = null){
        if($customer = $this->getDoctrine()->getRepository('ZeroBundle:Customer')
                            ->findOneBy(array('username' => $customer)))
            return $this->redirectToRoute('edit_customer', array('customer' => $customer->getUsername()));
        else if(!empty($request->get('username'))) {
            $customer = $this->getDoctrine()->getRepository('ZeroBundle:Customer')
                ->findOneBy(array('username' => $request->get('username')));
            return $this->redirectToRoute('edit_customer', array('customer' => $customer->getUsername()));
        }else
            return $this->redirectToRoute('list_customers');
    }

//    public function saveCategoryAction(Request $request){
//
//    }

//    public function editProductAction(Request $request){
//
//    }

//    public function editCustomerAction(Request $request){
//
//    }

    public function editDealAction(Request $request, $id = null){
        $em = $this->getDoctrine()->getManager();
        if(!$deal = $em->getRepository('ZeroBundle:Deal')->find($id)){
            $this->addFlash('error', 'Искомого Вами заказа не существует');
            return $this->redirectToRoute('list_deals');
        }

        $form = $this->createFormBuilder()
            ->add('make', SubmitType::class, array('label' => 'Офромить'))
            ->add('reject', SubmitType::class, array('label' => 'Отказать'))
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            //TODO Последовательность действий при подтверждении или отмене сделки.
            if($form->get('make')->isClicked()){
                $deal->setStatus(1);
                $em->flush();
                $em->persist($deal);
                $this->addFlash('notice', 'Сделка №'.$deal->getId().' подтверждена.');
                return $this->redirectToRoute('list_deals');
            }
            if($form->get('reject')->isClicked()){
                foreach($deal->getProducts() as $ptd){
                    $em->remove($ptd);
                }
                $em->remove($deal);
                $em->flush();
                $this->addFlash('error', 'Сделка №'.$deal->getId().' отвержена.');
                return $this->redirectToRoute('list_deals');
            }
        }

        return $this->render('ZeroBundle:Admin:editDeal.html.twig', array('form' => $form->createView(),
                                                                          'deal' => $deal));
    }

    public function editFeedbackAction(Request $request, $id = null){
        $em = $this->getDoctrine()->getManager();
        if(!$feedback = $em->getRepository('ZeroBundle:Feedback')->find($id)){
            $this->addFlash('error', 'Искомого Вами обращения не существует');
            return $this->redirectToRoute('list_feedbacks');
        }
        $form = $this->createFormBuilder()
            ->add('answer', TextareaType::class, array('attr' => array('class' => 'tinymce'),
                                                       'label' => 'Сообщение пользователю',
                                                       'required' => true))
            ->add('submit', SubmitType::class, array('label' => 'Отправить'))
            ->setMethod('POST')
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $feedback->setStatus('1');
            $message = \Swift_Message::newInstance()
                ->setSubject('Служба поддержки пользователей smesiproffi.ru')
                ->setFrom('feedback@smesiproffi.ru')
                ->setTo($feedback->getEmail())
                ->setBody(
                    $this->renderView(
                        'Emails/feedback.html.twig',
                        array('username' => $feedback->getName(),
                              'content' => $feedback->getContent(),
                              'answer' => $form->get('answer')->getData())
                    ),
                    'text/html'
                );
            $this->get('mailer')->send($message);
            $feedback->setStatus('1');
            $em->persist($feedback);
            $em->flush();
            return $this->redirectToRoute('list_feedbacks');
        }

        return $this->render('ZeroBundle:Admin:editFeedback.html.twig',
                             array('feedback' => $feedback,
                                   'form' => $form->createView()));
    }

    public function editCustomerAction(Request $request, $customer){
        $em = $this->getDoctrine()->getManager();
        if(!$user = $em->getRepository('ZeroBundle:Customer')
            ->findOneBy(array('username' => $customer)))
            $this->redirectToRoute('proffi_404');
        $form = $this->createForm(editCustomerType::class, $user);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            if($form->get('username')->getData() != $user->getUsername()){
                $user->setUsername($form->get('username')->getData());
            }
            if(!empty($form->get('password'))){
                $user->setPassword($this->get('security.password_encoder')
                                           ->encodePassword($user, $form->get('password')->getData()));
            }
            if($form->get('email')->getData() != $user->getEmail()){
                $user->setEmail($form->get('email')->getData());
            }
            if($form->get('phone')->getData() != $user->getPhone()){
                $user->setPhone($form->get('phone')->getData());
            }
            if($form->get('fullname')->getData() != $user->getFullname()){
                $user->setFullname($form->get('fullname')->getData());
            }

            $em->persist($user);
            $em->flush();
            $this->addFlash('notice', 'Пользователь изменён.');
            return $this->redirectToRoute('list_customers');
        }

        return $this->render('ZeroBundle:Admin:editCustomer.html.twig', array('customer' => $user,
                                                                                     'form' => $form));
    }

    public function addToSliderAction(Request $request, $id = null){
        if($id === null){
            $referer = $request->headers->get('referer');
            $url = $this->get('router')->generate($referer);
            return $this->redirectToRoute($url);
        }
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository('ZeroBundle:Product')->find($id);
        $product->setForslider();
        $em->persist($product);
        $em->flush();
        return new Response(json_encode('slider', $product->getForSlider()),200, array(''));
    }

    public function renderLeftMenuAction(){

    }

    public function addSuperCategoryAction(Request $request){
        $em = $this->getDoctrine()->getManager();

        $super = new SuperCategory();
        $form = $this->createForm(SuperCategoryType::class, $super);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $em->persist($super);
            $em->flush();
        }

        return $this->render('ZeroBundle:Admin:add_super_category.html.twig', array('form' => $form->createView()));
    }

    public function listDeliveryAction(Request $request = null, $id = null){
        $em = $this->getDoctrine()->getManager();

        $deliveries = $em->getRepository('ZeroBundle:Delivery')->findAll();

        return $this->render('ZeroBundle:Admin:list_deliveries.html.twig',
                             array('deliveries' => $deliveries));
    }

    public function editDeliveryAction(Request $request, $id = null){
        $em = $this->getDoctrine()->getManager();
        if(null !== $id){
            try {
                $delivery = $em->getRepository('ZeroBundle:Delivery')->find($id);
            }catch (NoResultException $e){
                $this->addFlash('error', 'Указанный в Вашем запросе ID способа доставки, отсутствует в БД');
                return $this->render('ZeroBundle:Admin:list_deliveries.html.twig');
            }
            $form = $this->createForm(DeliveryType::class, $delivery);

            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid()){
                $delivery->setName($form->get('name')->getData());
                $delivery->setPrice($form->get('price')->getData());
                $em->persist($delivery);
                $em->flush();
                $flash = 'Создан новый спосооб доставки '.$delivery->getName();
                $this->addFlash('notice', $flash);

                $deliveries = $em->getRepository('ZeroBundle:Delivery')->findAll();

                return $this->render('ZeroBundle:Admin:list_deliveries.html.twig',
                                     array('deliveries' => $deliveries));
            }

        }else {
            $delivery = new Delivery();

            $form = $this->createForm(DeliveryType::class, $delivery);

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $delivery->setName($form->get('name')->getData());
                $delivery->setPrice($form->get('price')->getData());
                $em->persist($delivery);
                $em->flush();
                $flash = 'Создан новый спосооб доставки ' . $delivery->getName();
                $this->addFlash('notice', $flash);

                $deliveries = $em->getRepository('ZeroBundle:Delivery')->findAll();

                return $this->render('ZeroBundle:Admin:list_deliveries.html.twig',
                                     array('deliveries' => $deliveries));
            }
        }

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $delivery->setName($form->get('name')->getData());
            $delivery->setPrice($form->get('price')->getData());
            $em->persist($delivery);
            $em->flush();
            $flash = 'Создан новый спосооб доставки ' . $delivery->getName();
            $this->addFlash('notice', $flash);
            $deliveries = $em->getRepository('ZeroBundle:Delivery')->findAll();

            return $this->render('ZeroBundle:Admin:list_deliveries.html.twig',
                                 array('deliveries' => $deliveries));
        }

        return $this->render('ZeroBundle:Admin:edit_delivery.html.twig',
                             array('form' => $form->createView()));
    }

    public function ajaxAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $productRepo = $em->getRepository('ZeroBundle:Product');
        $categoryRepo = $em->getRepository('ZeroBundle:Category');
        $superCatRepo = $em->getRepository('ZeroBundle:SuperCategory');
        $customerRepo = $em->getRepository('ZeroBundle:Customer');
        $dealRepo = $em->getRepository('ZeroBundle:Deal');
        $deliveryRepo = $em->getRepository('ZeroBundle:Delivery');
        $manufacturerRepo = $em->getRepository('ZeroBundle:Manufacturer');
        $feedbackRepo = $em->getRepository('ZeroBundle:Feedback');
        $trigger = $request->request->has('trigger') ? $request->request->get('trigger') : null;
        $action = $request->request->has('action') ? $request->request->get('action') : null;

        switch($trigger){
            case 'user':
                switch($action){
                    case 'new':

                    case 'edit':

                    case 'delete':

                    case 'copy':

                }
                break;
            case 'product':
                $pId = $request->request->has('p_id') ? $request->request->get('p_id') : null;
                if(isset($pId)){
                    $product = $productRepo->find($pId);
                }else{
                    $product = new Product();
                }
                $pName = $request->request->has('p_name') ? $request->request->get('p_name') : null;
                $pDesc = $request->request->has('p_description') ? $request->request->get('p_description') : null;
                $pUnit = $request->request->has('p_unit') ? $request->request->get('p_unit') : null;
                $pCatId = $request->request->has('p_category') ? $request->request->get('p_category') : null;
                $pManuf = $request->request->has('p_manufacturer') ? $request->request->get('p_manufacturer') : null;
                $pPromoted = $request->request->has('p_promoted') ? 1 : null;
                $pBuddyLike = $request->request->has('p_buddyLike') ? 1 : null;
                $pAlias = $request->request->has('p_alias') ? $request->request->get('p_alias') : null;
                $pIntroText = $request->request->has('p_introText') ? $request->request->get('p_introText') :
                    null;
                $pAllText = $request->request->has('p_allText') ? $request->request->get('p_allText') : null;
                $pKeyWords = $request->request->has('p_keywords') ? $request->request->get('p_keywords') : null;
                $pPrice = $request->request->has('p_price') ? $request->request->get('p_price') : null;
                $pDiscount = $request->request->has('p_discount') ? $request->request->get('p_discount') : null;
                if(isset($pCatId)) $category = $em->getRepository('ZeroBundle:Category')->find($pCatId);
                if(isset($pManuf)) $manufacturer = $manufacturerRepo->find($pManuf);
                $pIntroImage = $request->files->has('p_introImage') ? $request->files->get('p_introImage') : null;

                switch($action) {
                    case 'new':
                        if(isset($pName)) $product->setName($pName);
                        if(isset($pAllText)) $product->setAllText($pAllText);
                        if(isset($pAlias)) $product->setAlias($pAlias);
                        if(isset($category)) $product->setCategory($category);
                        if(isset($manufacturer)) $product->setManufacturer($manufacturer);
                        if(isset($pDesc)) $product->setDescription($pDesc);
                        if(isset($pIntroText)) $product->setIntroText($pIntroText);
                        if(isset($pKeyWords)) $product->setKeywords($pKeyWords);
                        if(isset($pPrice)) {
                            $product->setPrice($pPrice);
                            $product->setCalculatedPrice();
                        }
                        if(isset($pUnit)) $product->setUnit($pUnit);
                        if(isset($pDiscount)) $product->setDiscount($pDiscount);
                        if(isset($pPromoted)) $product->setPromoted($pPromoted);
                        if(isset($pBuddyLike)) $product->setBuddyLike($pBuddyLike);
                        if(isset($pIntroImage)){
                            $upDir = 'images/Store/Product/';
                            $imageName = md5(uniqid().$pIntroImage->getClientOriginalName().'
                            .'.$pIntroImage->getClientOriginalExtension());
                            $pIntroImage->move($upDir, $imageName);
                            $product->setIntroImg($imageName);
                        }

                        $em->persist($product);
                        $em->flush();
                        return new Response($product->toJson(), Response::HTTP_OK,
                                            array('Content/type' => 'application/json'));
                        break;
                    case 'edit':
                        if(isset($pId)) $product = $productRepo->find($pId);
                        if(isset($pName) && isset($product)) $product->setName($pName);
                        if(isset($pAllText) && isset($product)) $product->setAllText($pAllText);
                        if(isset($pAlias) && isset($product)) $product->setAlias($pAlias);
                        if(isset($pCatId) && isset($product)) $product->setCategory($category);
                        if(isset($pManuf) && isset($product)) $product->setManufacturer($manufacturer);
                        if(isset($pDesc) && isset($product)) $product->setDescription($pDesc);
                        if(isset($pIntroText) && isset($product)) $product->setIntroText($pIntroText);
                        if(isset($pKeyWords) && isset($product)) $product->setKeywords($pKeyWords);
                        if(isset($pPrice) && isset($product)) {
                            $product->setPrice($pPrice);
                            $product->setCalculatedPrice();
                        }
                        if(isset($pUnit)) $product->setUnit($pUnit);
                        if(isset($pDiscount) && isset($product)) $product->setDiscount($pDiscount);
                        if(isset($pPromoted) && isset($product)) $product->setPromoted($pPromoted);
                        if(isset($pBuddyLike) && isset($product)) $product->setBuddyLike($pBuddyLike);
                        if(isset($pIntroImage) && isset($product)){
                            $upDir = 'images/Store/Product/';
                            $imageName = md5(uniqid().$pIntroImage->getClientOriginalName().'
                            .'.$pIntroImage->getClientOriginalExtension());
                            $pIntroImage->move($upDir, $imageName);
                            $product->setIntroImg($imageName);
                        }
                        if(isset($product)){
                            $em->persist($product);
                            $em->flush();
                        }
                        return new Response($product->toJson(), Response::HTTP_OK,
                                                       array('Content/type' => 'application/json'));
                        break;
                    case 'delete':
                        if(isset($pId)) {
                            $product = $productRepo->find($pId);
                            $em->remove($product);
                            $em->flush();
                        }

                        return new Response(json_encode(array('delete' => true)), Response::HTTP_OK,
                                            array('Content/type' => 'application/json'));
                        break;
                    case 'copy':
                        if(isset($pId)) {
                            $product    = $productRepo->find($pId);
                            $newProduct = $this->get('globals_helper')->copy($product);
                            return new Response($newProduct->toJson(), Response::HTTP_OK, array('Content/type' => 'application/json'));
                        }
                        break;
                    case 'get':
                        if(isset($pId)) {
                            $product = $productRepo->find($pId);
                            $result  = $product->toJson();
                            return new Response($result, Response::HTTP_OK,
                                                array('Content/type' => 'application/json'));
                        }
                        break;
                }
                break;
            case 'super_cat':
                $sCatId = $request->request->has('s_id') ? $request->request->get('s_id') : null;
                $sCatName = $request->request->has('s_name') ? $request->request->get('s_name') : null;
                $sCatAlias = $request->request->has('s_alias') ? $request->request->get('s_alias') : null;
                $sCatDescription = $request->request->has('s_desc') ? $request->request->get('s_desc') : null;
                switch($action) {
                    case 'new':
                        $sCat = new SuperCategory();
                        if(isset($sCatName)) $sCat->setName($sCatName);
                        if(isset($pAlias)) $sCat->setAlias($sCatAlias);
                        if(isset($pAlias)) $sCat->setDescription($sCatDescription);
                        $em->persist($sCat);
                        $em->flush();
                        $response = array('id' => $sCat->getId(),
                                          'name' => $sCat->getName());
                        return new Response(json_encode($response), Response::HTTP_OK,
                                            array('Content/type' => 'application/json'));

                    case 'edit':
                        if(isset($sCatId)) {
                            $sCat = $superCatRepo->find($sCatId);
                            if (isset($sCatName))
                                $sCat->setName($sCatName);
                            if (isset($pAlias))
                                $sCat->setAlias($sCatAlias);
                            if (isset($pAlias))
                                $sCat->setDescription($sCatDescription);
                            $em->persist($sCat);
                            $em->flush();
                            $response = array('id' => $sCat->getId(),
                                              'name' => $sCat->getName());
                            return new Response(json_encode($response), Response::HTTP_OK,
                                                array('Content/type' => 'application/json'));
                        }
                        break;
                    case 'delete':
                        if(isset($sCatId)) {
                            $sCat = $superCatRepo->find($sCatId);
                            $id = $sCat->getId();
                            $name = $sCatId->getName();
                            $em->remove($sCat);
                            $em->flush();
//                            $response = array();
//                            $response['deleted'] = $sCat->getName();
//                            $s_categorie = $superCatRepo->findAll();
//                            foreach($s_categorie as $s){
//                            }
                            return new Response(json_encode(array('id' => $id,
                                                                  'name' => $name)),
                                                Response::HTTP_OK,
                                                array('Content/type' => 'application/json'));
                        }
                        break;
                    case 'copy':
                        break;
                    case 'get':
                        $sCat = $superCatRepo->find($sCatId);
                        $response = array();
                        $response['s_name'] = $sCat->getName();
                        $response['s_desc'] = $sCat->getDescription();
                        $response['s_alias'] = $sCat->getAlias();
                        return new Response(json_encode($response), Response::HTTP_OK, array('content/type' =>
                                                                                             'application/json'));
                        break;
                }
                break;
            case 'category':
                $catId = $request->request->has('cat_id') ? $request->request->get('cat_id') : null;
                if (isset($catId))
                    $category = $categoryRepo->find($catId);
                else
                    $category = new Category();
                $catName = $request->request->has('cat_name') ? $request->request->get('cat_name') : null;
                $catAlias = $request->request->has('cat_alias') ? $request->request->get('cat_alias') : null;
                $catDescription = $request->request->has('cat_description') ? $request->request->get
                ('cat_description') : null;
                $catKeyWords = $request->request->has('cat_keywords') ? $request->request->get('cat_keywords') : null;
                $catIntroText = $request->request->has('cat_introText') ? $request->request->get('cat_introText') :
                    null;
                $catAllText = $request->request->has('cat_allText') ? $request->request->get('cat_allText') : null;
                $catIntroImage = $request->files->has('cat_introImg') ? $request->files->get('cat_introImg')
                    : null;
                $catSCat = $request->request->has('cat_s_cat') ? $superCatRepo->find($request->request->get
                ('cat_s_cat')) : null;

                switch($action) {
                    case 'new':
                        $category->setDescription($catDescription);
                        $category->setName($catName);
                        $category->setIntroText($catIntroText);
                        $category->setKeywords($catKeyWords);
                        $category->setAllText($catAllText);
                        $category->setAlias($catAlias);
                        $category->setSupercategory($catSCat);
                        $upDir = 'images/Store/Category';
                        if(isset($catIntroImage)){
                            $imageName = md5(uniqid().$catIntroImage->getClientOriginalName()).'.'
                                .$catIntroImage->getClientOriginalExtension();
                            $catIntroImage->move($upDir, $imageName);
                            $category->setImage($imageName);
//                            print_r($catIntroImage);
                        }
                        $em->persist($category);
                        $em->flush();
                        return new Response(json_encode(array('id' => $category->getId(),
                                                              'name' => $category->getName())),
                                                Response::HTTP_OK, array('Content/type' => 'application/json'));
//                        print_r($_FILES);
//                        return new Response(json_encode($request->files->all()),
//                                                Response::HTTP_OK, array('Content/type' => 'application/json'));
                        break;
                    case 'edit':
                        $category->setDescription($catDescription);
                        $category->setName($catName);
                        $category->setIntroText($catIntroText);
                        $category->setKeywords($catKeyWords);
                        $category->setAllText($catAllText);
                        $category->setAlias($catAlias);
                        $category->setSupercategory($catSCat);
                        $upDir = '/images/Store/Category';
                        if(isset($catIntroImage)){
                            $imageName = md5(uniqid()).'_'.
                                $catIntroImage['intro_img']['tmp_name'];
                            move_uploaded_file($imageName, $upDir);
                            $category->setIntroImg($imageName);
                        }
                        $em->persist($category);
                        $em->flush();
                        return new Response(json_encode(array('id' => $category->getId(),
                                                              'name' => $category->getName())),
                                            Response::HTTP_OK, array('Content/type' => 'application/json'));
                        break;
                    case 'delete':
                        $id = $category->getId();
                        $name = $category->getName();
                        $em->remove($category);
                        $em->flush();
                        return new Response(json_encode(array('id' => $id,
                                                              'name' => $name), Respons_categoryse::HTTP_OK,
                                                        array('Content/type' => 'application/json')));
                        break;
                    case 'copy':
                        break;
                    case 'get':
                    if(isset($catId)) {
                        $category = $categoryRepo->find($catId);
                        $result = $category->toJson();
                        return new Response($result, Response::HTTP_OK,
                                            array('Content/type' => 'application/json'));
                    }
                    break;
                    }
                break;
            case 'deal':
                switch($action) {
                    case 'new':

                    case 'edit':

                    case 'delete':

                    case 'copy':
                }
                break;

            case 'manufacturer':
                $mId = $request->request->has('m_id') ? $request->request->get('m_id') : null;
                if(isset($mId))
                    $manufacturer = $manufacturerRepo->find($mId);
                else
                    $manufacturer = new Manufacturer();
                $mDescription = $request->request->has('m_description') ? $request->request->get('m_description') :
                    null;
                $mName = $request->request->has('m_name') ? $request->request->get('m_name') : null;
                $mLegalEntity = $request->request->has('m_legal_entity') ? $request->request->get('m_legal_entity') :
                    null;
                $mLink = $request->request->has('m_link') ? $request->request->get('m_link') : null;
                $mLogo = $request->files->has('m_logo') ? $request->files->get('m_logo') : null;
                switch($action) {
                    case 'new':
                        if(isset($manufacturer)){
                            if(isset($mName)) $manufacturer->setName($mName);
                            if(isset($mDescription)) $manufacturer->setDescription($mDescription);
                            if(isset($mLegalEntity)) $manufacturer->setLegalEntity($mLegalEntity);
                            if(isset($mLink)) $manufacturer->setLink($mLink);
                            if(isset($mLogo)){
                                $upDir = '/images/Store/Manufacturer/';
                                $imageName = md5(uniqid().$mLogo->getClientOriginalName()).'.'
                                    .$mLogo->getClientOriginalExtension();
                                $mLogo->move($upDir, $imageName);
                                $manufacturer->setLogo($imageName);
                            }
                            $em->persist($manufacturer);
                            $em->flush();
                            return new Response($manufacturer->toJson(), Response::HTTP_OK, array(
                                'Content/type' => 'application/json'));
                        }
                        break;
                    case 'edit':
                        if(isset($manufacturer)){
                            if(isset($mName)) $manufacturer->setName($mName);
                            if(isset($mDescription)) $manufacturer->setDescription($mDescription);
                            if(isset($mLegalEntity)) $manufacturer->setLegalEntity($mLegalEntity);
                            if(isset($mLink)) $manufacturer->setLink($mLink);
                            if(isset($mLogo)){
                                $upDir = '/images/Store/Manufacturer/';
                                $imageName = md5(uniqid().$mLogo->getClientOriginalName()).'.'
                                    .$mLogo->getClientOriginalExtension();
                                $mLogo->move($upDir, $imageName);
                                $manufacturer->setLogo($imageName);
                            }
                            $em->persist($manufacturer);
                            $em->flush();
                            return new Response($manufacturer->toJson(), Response::HTTP_OK, array(
                                'Content/type' => 'application/json'));
                        }
                        break;
                    case 'delete':
                        if(isset($manufacturer)){
                            $id = $manufacturer->getId();
                            $name = $manufacturer->getName();
                            $em->remove($manufacturer);
                            $em->flush();
                            return new Response(array('id' => $id,
                                                      'name' => $name), Response::HTTP_OK, array(
                                'Content/type' => 'application/json'));
                        }
                    case 'copy':
                        break;
                    case 'get':
                        if(isset($manufacturer)){
                            return new Response($manufacturer->toJson(), Response::HTTP_OK, array(
                                'Content/type' => 'application/json'));
                        }
                }
                break;
        }
    }
}
