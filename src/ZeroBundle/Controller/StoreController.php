<?php

namespace ZeroBundle\Controller;

use Doctrine\ORM\NoResultException;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use ZeroBundle\Entity\ProductsToCart;
use ZeroBundle\Entity\Cart;
use Doctrine\ORM\Tools\Pagination\Paginator;



class StoreController extends Controller
{
	public function indexAction()
	{
		$repo = $this->getDoctrine()->getRepository('ZeroBundle:Product');
		$promoted = $repo->findPromotedProducts();
		return $this->render('ZeroBundle:Default:index.html.twig',
	                     array('promoted' => $promoted));
	}

	public function buddyAction(Request $request){
		if(!$q = $request->request->get('buddy')){
			throw new \Error('Неверный запрос на данные приятеля');
		}
		//TODO развить эту тупую мысль.
	}

	public function ajaxChoiceAction(Request $request){
		$em = $this->getDoctrine()->getManager();
		$filter = $request->request->has('filter') ? $request->request->get('filter') : '';
		if($filter == 'filter') {
			$order    = $request->request->has('order') ? $request->request->get('order') : '';
			$orderDir = $request->request->has('dir') ? $request->request->get('dir') : 'ASC';
			$category = $request->request->has('category') ? $request->request->get('category') : '';
			$manufacturer = $request->request->has('manufacturer') ? $request->request->get('manufacturer') : '';
			$min_p    = $request->request->has('min_p') ? $request->request->get('min_p') : '';
			$max_p    = $request->request->has('max_p') ? $request->request->get('max_p') : '';
			$like     = $request->request->has('like') ? $request->request->get('like') : '';

			$options = array();

			if($order) $options['order'] = $order;
			if($order) $options['dir'] = $orderDir;
			if($category) $options['category'] = $category;
			if($manufacturer) $options['manufacturer'] = $manufacturer;
			if($min_p) $options['min_p'] = $min_p;
			if($max_p) $options['max_p'] = $max_p;
			if($like) $options['like'] = $like;

			$products = $em->getRepository('ZeroBundle:Product')->findFilteredProducts($options);
			$c = 0;
			$response = array();
			foreach ($products as $product) {
				$buddyLike = $product->getBuddyLike();
				$productLink = $this->get('router')
							->generate('_product_page',
							           array('category' => $product->getCategory()->getSuperCategory()->getAlias(),
							                 'alias' => $product->getCategory()->getAlias(),
							                 'id' => $product->getId()));
				$imageSrc = '/images/Store/Product/'.$product->getIntroImg();
				$response[$c] = array(
					'type' => 'product',
					'buddyLike' => $buddyLike,
					'productLink' => $productLink,
					'imageSrc' => $imageSrc,
					'name' => $product->getName(),
					'price' => $product->getCalculatedPrice(),
					'id' => $product->getId(),
					'introText' => $product->getIntroText(),
				  'calculatedPrice' => $product->getCalculatedPrice(),
					'unit' => $product->getUnit()
				);
				$c++;
			}
//			$response['min_p'] = $em->getRepository('ZeroBundle:Product')->returnMinPrice($category);
//			$response['max_p'] = $em->getRepository('ZeroBundle:Product')->returnMaxPrice($category);
			$response = json_encode($response);
			return new Response($response, Response::HTTP_OK, array('content-type' => 'application/json'));

		}else if($filter == 'get_cats'){
			$scat_id = $request->request->has('super_category') ? $request->request->get('super_category')
					: null;
			if($scat_id != null && $scat_id != 0) {
				$scategory = $em->getRepository('ZeroBundle:SuperCategory')->find($scat_id);
				$categories = $scategory->getCategories();
				$response = array();
				foreach($categories as $category){
					$response[$category->getId()] = $category->getName();
				}
//				$array = array();
//				foreach($scategory->getCategories() as $item){
//					$array[] = $item->getId();
//				}
//				$response['min_p'] = $em->getRepository('ZeroBundle:Product')->returnMinPrice($array);
//				$response['max_p'] = $em->getRepository('ZeroBundle:Product')->returnMaxPrice($array);
				$response = json_encode($response);
			}else{
				$response = json_encode(array('nocategories' => true));
			}
//
			return new Response($response, Response::HTTP_OK, array('content-type' => 'application/json'));
		}
	}

	public function listCategoriesAction(Request $request, $category){
		$em = $this->getDoctrine()->getManager();
		$category = $em->getRepository('ZeroBundle:SuperCategory')->findOneBy(array('alias' => $category));
		$catId = $category->getId();
		$query = $em->createQueryBuilder()
				->select('c')
				->from('ZeroBundle:Category', 'c')
				->where('c.supercategory = :catId')
				->setParameter('catId', $catId);
		$paginator = $this->get('knp_paginator');
		$pagination = $paginator->paginate(
				$query,
				$request->query->getInt('page', 1),
				10
		);

		return $this->render('ZeroBundle:Default:list_categories.html.twig', array('pagination' => $pagination,
		                                                                           'category' => $category));
	}

	public function showCategoryAction(Request $request, $alias = null){
		$em = $this->getDoctrine()->getManager();
		$category = $em->getRepository('ZeroBundle:Category')->findOneBy(array('alias' => $alias));
		$catId = $category->getId();
		$query = $em->createQueryBuilder()
				->select('p')
				->from('ZeroBundle:Product', 'p')
				->where('p.category = :catId')
				->setParameter('catId', $catId);
		$paginator = $this->get('knp_paginator');
		$pagination = $paginator->paginate(
				$query,
				$request->query->getInt('page', 1),
				10
		);
		return $this->render('ZeroBundle:Default:category.html.twig', array('pagination' => $pagination,
		                                                                    'category' => $category));
	}

	/**
	 *
	 * @param Request|null $request
	 * @param              $alias
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
		//TODO Доделать
		public function showProductAction(Request $request = null, $category = null, $alias = null, $id = null){
			$em = $this->getDoctrine()->getManager();
			try{
				$product = $this->getDoctrine()
						->getRepository('ZeroBundle:Product')->find($id);
			}catch(NoResultException $e){
				return $this->render('proffi_404');
			}
				$product->setClickCounter();
				$em->persist($product);
				$em->flush();
				$category = $product->getCategory();
//				$allProducts = $category->getProducts();
				$allProducts = $em->getRepository('ZeroBundle:Product')->findPromotedInCategory($product->getCategory()
						                                                                                ->getId(),
				                                                                                $product->getId());

				$form = $this->createFormBuilder()
												->add('quantity', IntegerType::class, array('label' => 'Сколько нужно?',
												                                            'data' => '1',
												                                            'attr' => array('class' => 'item-quantity',
												                                                            'autofocus' => '',
												                                                            'min' => 1)))
												->add('product', HiddenType::class, array('data' => $product->getId()))
												->add('add', SubmitType::class, array('label' => 'Купить',
												                                      'attr' => array('fab' => '',
												                                                      'class' => 'addtocart',
												                                                      'bg-pink600' => '',
												                                                      'z-1' => '')))
												->getForm();

				$formView = $form->createView();

				$session = new Session();

				$form->handleRequest($request);

				if($form->isSubmitted() && $form->isValid()){

						if (!$session->has('cart')) {
							$cart = new Cart();
							$em->persist($cart);
							$em->flush();
							$session->set('cart', $cart->getId());
						}else{
								if(!$cart = $em->getRepository('ZeroBundle:Cart')->findOneBy(array('id' => $session->get
								('cart')))){
									$cart = new Cart();
									$em->persist($cart);
									$em->flush();
									$session->remove('cart');
									$session->set('cart', $cart->getId());
								}
						}

					$product = $em->getRepository('ZeroBundle:Product')
							->findOneBy(array('id' => $form->get('product')->getData()));

					if(!$productToCart = $this->getDoctrine()
							->getRepository('ZeroBundle:ProductsToCart')
							->checkProductInCart($cart, $product))
									$productToCart = new ProductsToCart();
					$productToCart->setCart($cart);
					$productToCart->setProduct($product);
					$productToCart->setQuantity($productToCart->getQuantity() + $form->get('quantity')->getData());

					$em->persist($cart);
					$em->persist($productToCart);
					$em->flush();

					return $this->redirectToRoute('_product_page', array('id' => $product->getId(), 'category' =>
							$product->getCategory()->getSupercategory()->getAlias(), 'alias' => $product->getCategory()->getAlias()));
				}

				return $this->render('ZeroBundle:Default:product.html.twig',
				                     array('product' => $product,
				                           'category' => $category,
				                           'promoted' => $allProducts,
				                           'form' => $formView));
		}
}
