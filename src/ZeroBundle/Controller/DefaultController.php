<?php

namespace ZeroBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ZeroBundle\Entity\Product;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    public function indexAction()
    {

        return $this->render('ZeroBundle:index.html.twig');
    }

    public function showProductAction($id){
        $product = $this->getDoctrine()->getRepository('ZeroBundle:Product')
            ->findOneByIdJoinedWithCategory($id);
        $category = $product->getCategory();

        return $this->render('ZeroBundle:Default:product.html.twig', array('product' => $product,
                                                                            'category' => $category));
    }
}
