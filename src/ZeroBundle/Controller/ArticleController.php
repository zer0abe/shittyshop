<?php

namespace ZeroBundle\Controller;

use Doctrine\ORM\NoResultException;
use ZeroBundle\Entity\Article;
use ZeroBundle\Entity\ArticleCategory;
use ZeroBundle\Entity\Image;
use ZeroBundle\Form\Type\ArticleType;
use ZeroBundle\Form\Type\ImageType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ArticleController extends Controller
{
    public function showAction($id)
    {
        try{
            $article = $this->getDoctrine()->getRepository('ZeroBundle:Article')->find($id);
        }catch(NoResultException $e){
            return $this->redirectToRoute('proffi_404');
        }

        return $this->render('ZeroBundle:Article:show.html.twig', array('article' => $article));
    }

    public function newCategoryAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $category = new ArticleCategory();

        $form = $this->createFormBuilder()
            ->add('name', TextType::class, array('label' => 'Название категории'))
            ->add('save', SubmitType::class, array('label' => 'Сохранить'))
            ->getForm();
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $category->setName($form->get('name')->getData());
            $em->persist($category);
            $em->flush();
            return $this->redirectToRoute('list_articles');
        }

        return $this->render('ZeroBundle:Admin:newArticleCategory.html.twig',
                             array('form' => $form->createView()));
    }

    public function newAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $article = new Article();

        $form = $this->createFormBuilder()
            ->add('name', TextType::class, array('label' => 'Название'))
            ->add('category', EntityType::class, array(
                'class' => 'ZeroBundle:ArticleCategory',
                'choice_label' => 'name',
                'label' => 'Категория'))
            ->add('content', TextareaType::class, array('label' => 'Наименование',
                                                        'attr' => array('class' => 'tinymce')))
            ->add('map', TextareaType::class, array('label' => 'Карта',
                                                    'required' => false))
            ->add('images', CollectionType::class, array('entry_type' => ImageType::class,
                                                         'allow_add' => true,
                                                         'prototype' => true,
//                                                         'multiple' => false,
                                                         'entry_options' => array('required' => false,
                                                                                  'data_class' => null,
                                                                                  //						                                                                      'multiple' => false)
                                                         )))
            ->add('save', SubmitType::class, array('label' => 'Сохранить'))
        ->getForm();

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            if($images = $form->get('images')->getData()) {
//                print_r($images);
                foreach ($images as $image) {
//                  print_r($image);
//                    print_r($image);
                    $tmp = new Image();
                    $tmp->setFile($image['file']);
                    $tmp->upload();
                    $em->persist($tmp);
                    $em->flush();
                    echo "1";
                    echo " 2";
                    $em->flush();
                    echo " 3";
                    $article->addImage($tmp);
                }
            }
            $article->setMap($form->get('map')->getData());
            $article->setName($form->get('name')->getData());
            $article->setCategory($form->get('category')->getData());
            $article->setContent($form->get('content')->getData());
            $em->persist($article);
            echo " 5";
            $em->flush();
            echo " 6";
            return $this->redirectToRoute('list_articles');
        }

        return $this->render('ZeroBundle:Admin:newArticle.html.twig',
                             array('form' => $form->createView()));
    }

    public function editAction(Request $request, $id = null){
        $em = $this->getDoctrine()->getManager();
        $article = $this->getDoctrine()->getRepository('ZeroBundle:Article')->find($id);

        $form = $this->createForm(ArticleType::class, $article);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $em -> persist($article);
            $em->flush();
            return $this->redirectToRoute('list_articles');
        }

        return $this->render('ZeroBundle:Admin:newArticle.html.twig',
                             array('form' => $form->createView()));
    }

    public function deleteAction($id){
        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository('ZeroBundle:Article')->find($id);

        $em->remove($article);
        $em->flush();

        return $this->redirectToRoute('list_articles');
    }

    public function listAction(){
        $articles = $this->getDoctrine()->getRepository('ZeroBundle:Article')->findAll();

        return $this->render('ZeroBundle:Admin:listArticles.html.twig',
                             array('articles' => $articles));
    }
}
