<?php

namespace ZeroBundle\Controller;

use Doctrine\ORM\NoResultException;
use ZeroBundle\Entity\Product;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use ZeroBundle\Entity\Cart;
use ZeroBundle\Entity\Customer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use ZeroBundle\Entity\ProductsToCart;
use ZeroBundle\Entity\Delivery;

class CartController extends Controller
{
		public function addToCartAction(Request $request, $id = null){
			$session = new Session();
			$em = $this->getDoctrine()->getManager();
			if (!$session->has('cart')) {
				$cart = new Cart();
				$em->persist($cart);
				$em->flush();
				$session->set('cart', $cart->getId());
			}else{
				$cart = $em->getRepository('ZeroBundle:Cart')->find($session->get('cart'));
			}

			$pId = $id ? $id : $request->request->get('pid');

			try{
				$product = $em->getRepository('ZeroBundle:Product')->find($pId);
			}catch(NoResultException $e){
				if($request->request->has('pid')){
					return new Response(json_encode(array('success' => false)),
					                    Response::HTTP_OK,
					                    array('Content/type' => 'application/json'));
				}else{
					$session->getFlashBag()->add('error', 'Вы пытались добавить в корзину несуществующий товар');
					return $this->redirectToRoute('home_page');
					}
			}
			if(!$productToCart = $this->getDoctrine()
					->getRepository('ZeroBundle:ProductsToCart')
					->checkProductInCart($cart, $product))
				$productToCart = new ProductsToCart();
			$productToCart->setCart($cart);
			$productToCart->setProduct($product);
			$productToCart->setQuantity($productToCart->getQuantity() + 1);

			$em->persist($cart);
			$em->persist($productToCart);
			$em->flush();
			$message = 'Вы добавили в корзину 1 ед. '.$product->getName().' по цене:'.$product->getCalculatedPrice().' р.';
			$session->getFlashBag()->add('notice', $message);

			if($request->request->has('pid')){
				return new Response(json_encode(array('success' => true)),
				                    Response::HTTP_OK,
				                    array('Content/type' => 'application/json'));
			}else
				return $this->redirect($request->headers->get('referer'));
		}

		public function createCartAction($customer_id) {

		}

		public function showCartAction(){
			$session = new Session();
			$em = $this->getDoctrine()->getManager();
				if (!$session->has('cart')) {
					$cart = new Cart();
					$em->persist($cart);
					$em->flush();
					$session->set('cart', $cart->getId());
				}else{
					$cart = $em->getRepository('ZeroBundle:Cart')->find($session->get('cart'));
				}
			$counter = 0;
			$products = $cart->getProducts();
			foreach($products as $p){
				if($p != null){
					$counter++;
				}
			}
			if($counter){
					$ptc = $cart->getProducts();
				$dealCost = 0;
				foreach($ptc as $product){
					$mult = $product->getQuantity() * $product->getProduct()->getCalculatedPrice();
					$dealCost += $mult;
				}
				return $this->render('ZeroBundle:Default:mycart.html.twig',
				                     array('cart' => $cart,
				                           'products' => $ptc,
				                           'costs' => $dealCost,
				                           'notfound' => false));
			}else{
				return $this->render('ZeroBundle:Default:mycart.html.twig',
				                     array(
						                     'notfound' => true));
			}
		}

		public function miniCartAction(){
			$session = new Session();
			$em = $this->getDoctrine()->getManager();
			if (!$session->has('cart')) {
				$cart = new Cart();
				$em->persist($cart);
				$em->flush();
				$session->set('cart', $cart->getId());
			}else{
				$cart = $em->getRepository('ZeroBundle:Cart')->find($session->get('cart'));
			}

			$counter = (null === $cart) ? null : count($cart->getProducts());
			$products = (null === $cart) ? null : $cart->getProducts();
			$cost = 0;
			if(null !== $products) {
				foreach ($products as $product) {
					$cost += $product->getQuantity() * $product->getProduct()->getCalculatedPrice();
				}
			}

			return $this->render('ZeroBundle:Cart:miniCart.html.twig', array('cart' => $cart,
			                                                                        'counter' => $counter,
			                                                                        'products' => $products,
			                                                                        'cost' => $cost));
		}

		public function renderProductPositionAction(Request $request = null, $product){
			$session = new Session();
			$em = $this->getDoctrine()->getManager();
			if (!$session->has('cart')) {
				$cart = new Cart();
				$em->persist($cart);
				$em->flush();
				$session->set('cart', $cart->getId());
			}else{
				$cart = $em->getRepository('ZeroBundle:Cart')->find($session->get('cart'));
			}
				$relatedProduct = $product->getProduct();
				$form = $this->createFormBuilder()
						->setAction($this->generateUrl('_cart_controll'))
						->setMethod('POST')
						->add('cartId', HiddenType::class, array('data' => $cart->getId()))
						->add('productId', HiddenType::class, array('data' => $product->getId()))
						->add('quantity', IntegerType::class, array('data' => $product->getQuantity(),
						                                         'label' => 'Кол-во',
						                                            'attr' => array('class' => 'cart-quantity')))
						->add('remove', SubmitType::class, array('label' => 'Удалить',
						                                         'attr' => array('class' => 'cart-remove')))
						->add('recount', SubmitType::class, array('label' => 'Пересчитать',
						                                          'attr' => array('class' => 'cart-recount')))
						->getForm();

				return $this->render('ZeroBundle:Default:productPosition.html.twig',
				                     array('form' => $form->createView(),
				                           'relatedProduct' => $relatedProduct,
				                           'product' => $product
				                     ));
//			}else{
//				return new Response('', 200);
//			}
		}

		public function productPositionControlAction(Request $request){
			$form = $request->request->get('form');
			if(array_key_exists('remove', $form)){
				$productId = $form['productId'];
				$em = $this->getDoctrine()->getManager();
				$productToCart = $this->getDoctrine()->getRepository('ZeroBundle:ProductsToCart')
						->find($productId);

				$em->remove($productToCart);
				$em->flush();
				return $this->redirectToRoute('_show_cart');
			}else if(array_key_exists('recount', $form)){
				$productId = $form['productId'];
				$quantity = $form['quantity'];
				$em = $this->getDoctrine()->getManager();

				$ptc = $this->getDoctrine()->getRepository('ZeroBundle:ProductsToCart')
						->find($productId);
				$ptc->setQuantity($quantity);
				$em->persist($ptc);
				$em->flush();
				return $this->redirectToRoute('_show_cart');
			}
		}

		public function makeADealAction(){
			$session = new Session();

			$em = $this->getDoctrine()->getManager();

			if(!$customer = $this->getUser()) {
				if (empty($session->get('cart')) || !$session->has('cart')) {
					$cart = new Cart();
					$em->persist($cart);
					$em->flush();
					$session->set('cart', $cart->getId());
				}else{
					$cart = $em->getRepository('ZeroBundle:Cart')->findOneBy(array('id' => $session->get('cart')));
				}
			}else if($session->has('cart')){
				$cart = $em->getRepository('ZeroBundle:Cart')->find($session->get('cart'));
				$customer->setCart($cart);
				$em->persist($customer);
				$em->flush();
			}else{
				if(!$cart = $customer->getCart()) {
					$cart = new Cart();
					$customer->setCart($cart);
					$em->persist($customer);
					$em->persist($cart);
					$em->flush();
					$session->set('cart', $cart->getId());
				}
			}

			$products = $this->getDoctrine()->getRepository('ZeroBundle:Cart')
					->getRelatedProducts($cart->getId());
//			$ptc = $this->getDoctrine()->getRepository('ZeroBundle:ProductsToCart')
//					->findBy(array('cart' => $cart->getId()));
			$ptc = $cart->getProducts();

			$dealCost = 0;
			foreach($ptc as $product){
				$mult = $product->getQuantity() * $product->getProduct()->getCalculatedPrice();
				$dealCost += $mult;
			}

			return $this->render('ZeroBundle:Default:mycartSubmit.html.twig',
			                     array('products' => $products,
			                           'cost' => $dealCost));
		}

		public function productPositionSubmitAction(Request $request, $product){
			$session = new Session();
			$em = $this->getDoctrine()->getManager();

			if(!$customer = $this->getUser()) {
				if (empty($session->get('cart')) || !$session->has('cart')) {
					$cart = new Cart();
					$em->persist($cart);
					$em->flush();
					$session->set('cart', $cart->getId());
				}else{
					$cart = $em->getRepository('ZeroBundle:Cart')->findOneBy(array('id' => $session->get('cart')));
				}
			}else if($session->has('cart')){
				$cart = $em->getRepository('ZeroBundle:Cart')->find($session->get('cart'));
				$customer->setCart($cart);
				$em->persist($customer);
				$em->flush();
			}else{
				if(!$cart = $customer->getCart()) {
					$cart = new Cart();
					$customer->setCart($cart);
					$em->persist($customer);
					$em->persist($cart);
					$em->flush();
					$session->set('cart', $cart->getId());
				}
			}
			$ptc = $this->getDoctrine()->getRepository('ZeroBundle:ProductsToCart')
					->findOneBy(array('product' => $product->getId(),
					                  'cart' => $cart->getId()));

			return $this->render('ZeroBundle:Default:productPositionSubmit.html.twig',
			                     array('product' => $product,
			                           'ptc' => $ptc));
		}

}
