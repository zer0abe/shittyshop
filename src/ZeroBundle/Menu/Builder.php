<?php

namespace ZeroBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

Class Builder implements ContainerAwareInterface
{
		use ContainerAwareTrait;

		public function mainMenu(FactoryInterface $factory, array $options){

				$menu = $factory->createItem('root');

				$em = $this->container->get('doctrine')->getManager();

				$sCategories = $em->getRepository('ZeroBundle:SuperCategory')
						->findAll();
				$menu->addChild('Каталог товаров', array('attributes'=>array('class' => 'top-level-menu-item')));

				foreach($sCategories as $s){
					$menu['Каталог товаров']->addChild($s->getName(), array('attributes'=>array('class' =>
							                                                                            'top-level-menu-item')));
					$cS = $s->getCategories();
					foreach($cS as $c){
						$menu['Каталог товаров'][$s->getName()]->addChild($c->getName(), array('route' => '_category_page',
						                                                                     'routeParameters' => array(
							                                                                   'alias' => $c->getAlias()),
						                                                                     'attributes'=>
								                                                                             array('class' =>
										                                                                                   'top-level-menu-item')));
					}
				}

				$articleCategories = $em->getRepository('ZeroBundle:ArticleCategory')->findAll();
				foreach($articleCategories as $category){
					$menu->addChild($category->getName(), array('attributes'=>array('class' => 'top-level-menu-item')));
					$articles = $category->getArticles();
					foreach($articles as $article){
						$menu[$category->getName()]->addChild($article->getName(), array('route' => 'show_article',
						                                                                 'routeParameters' => array(
							                                                                 'id' => $article->getId()),
						                                                                 'attributes'=>array('class' => 'menu-item')));
					}
				}


				return $menu;
		}
}
