<?php
namespace ZeroBundle\EventListener;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Bundle\DoctrineBundle\Registry as Doctrine;

class ymlGenerator
{
	/*
	 * Service container
	 */
	private $container;

	/*
	 * Entity Manager
	 */
	private $em;

	/*
	 * Doctrine
	 */
	private $doctrine;

	/*
	 * Конструктор объекта
	 */
	public function __construct(Doctrine $doctrine, ContainerInterface $container) {
		$this->doctrine = $doctrine;
		$this->container = $container;
		$this->em = $this->doctrine->getManager();
	}

	public function generate(){
		$categories = $this->em->getRepository('ZeroBundle:Category')->findAll();

	}
}
