<?php

namespace ZeroBundle\EventListener;

use ZeroBundle\Entity\Cart;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Doctrine\Bundle\DoctrineBundle\Registry as Doctrine; // for Symfony 2.1.0+
// use Symfony\Bundle\DoctrineBundle\Registry as Doctrine; // for Symfony 2.0.x
/**
 * Custom login listener.
 */
class LoginListener
{

		private $container;
		/** @var \Symfony\Component\Security\Core\SecurityContext */
		private $securityContext;

		/** @var \Doctrine\ORM\EntityManager */
		private $em;

		private $request;

		private $router;

	/**
	 * Constructor
	 *
	 * @param RequestStack         $request
	 * @param ContainerInterface   $container
	 * @param AuthorizationChecker $security
	 * @param Doctrine             $doctrine
	 * @param                      $router
	 *
	 * @internal param SecurityContext $securityContext
	 */
		public function __construct(RequestStack $request, ContainerInterface $container,
		                            AuthorizationChecker $security,
		                            Doctrine $doctrine)
		{
				$this->container = $container;
				$this->security = $security;
				$this->em = $doctrine->getManager();
				$this->request = $request;
				$this->router = $this->container->get('router');
		}

	/**
	 * Do the magic.
	 *
	 * @param InteractiveLoginEvent $event
	 *
	 * @return RedirectResponse
	 */
		public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
		{
				if ($this->security->isGranted('IS_AUTHENTICATED_FULLY')) {
						$session = new Session();
						$user = $event->getAuthenticationToken()->getUser();
						$user->setLastVisit(new \DateTime());
						if($session->has('cart')){
							if($cart = $user->getCart()) {
								$user->setCart(null);
								$this->em->persist($user);
								$this->em->flush();
								foreach($cart->getProducts() as $product) {
									$this->em->remove($product);
								}
								$this->em->remove($cart);
								$this->em->flush();
//								$session->remove('cart');
							}
							$cart = $this->em->getRepository('ZeroBundle:Cart')->find($session->get('cart'));
							$user->setCart($cart);
							$cart->setCustomer($user);
						}
						$this->em->persist($user);
						$this->em->persist($cart);
						$this->em->flush();
						$referer = $this->request->getCurrentRequest()->headers->get('referer');
						if(!strpos($referer, 'login'))
							$url = $referer;
						else $url = $this->router->generate('_homepage');
						if($session->has('referer')) {
							$url = $session->get('referer');
							$session->remove('referer');
							return new RedirectResponse($url, 302);
						}
						return new RedirectResponse($url, 302);
				}

				if ($this->security->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
						// user has logged in using remember_me cookie
				}

				// do some other magic here
//				$user = $event->getAuthenticationToken()->getUser();

				// ...
		}
}
