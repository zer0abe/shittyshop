<?php

namespace ZeroBundle\EventListener;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Http\Logout\LogoutSuccessHandlerInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Doctrine\Bundle\DoctrineBundle\Registry as Doctrine; // for Symfony 2.1.0+
// use Symfony\Bundle\DoctrineBundle\Registry as Doctrine; // for Symfony 2.0.x
/**
 * Custom login listener.
 */
class LogoutListener implements LogoutSuccessHandlerInterface
{
		/** @var \Symfony\Component\Security\Core\SecurityContext */
		private $securityContext;

		/** @var \Doctrine\ORM\EntityManager */
		private $em;

		private $storage;

		private $session;

		/**
		 * Constructor
		 *
		 * @param SecurityContext $securityContext
		 * @param Doctrine        $doctrine
		 */
		public function __construct(TokenStorage $storage, Doctrine $doctrine)
		{
				$session = new Session();
				$this->storage = $storage;
				$this->em = $doctrine->getManager();
				$this->session = $session->get('cart');
		}

	/**
	 * Do the magic.
	 *
	 * @param Request $request
	 *
	 * @return \Symfony\Component\HttpFoundation\Response|void
	 * @internal param InteractiveLoginEvent $event
	 */
		public function onLogoutSuccess(Request $request)
		{

						$session = new Session();
						$user = $this->storage->getToken()->getUser();
						$session->set('cart', $this->session);
						if($session->has('referer'))
							$session->remove('referer');


						return new Response();
		}
}
