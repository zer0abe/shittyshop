<?php

namespace ZeroBundle\EventListener;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Вылавливаем реферера на определённых событиях и сохраняем его в сессии
 * для дальнейшего использования
 */
class RefererHelper
{
		private $request;

	/**
	 * Constructor
	 *
	 * @param RequestStack $request
	 *
	 */
		public function __construct(RequestStack $request)
		{
				$this->request = $request->getCurrentRequest();
		}

	/**
	 * Do the magic.
	 *
	 */
		public function storeReferer(){
			$session = new Session();
			if($session->has('referer')){
				$session->remove('referer');
			}
			$referer = $this->request->headers->get('referer');
			$session->set('referer', $referer);
		}
}
