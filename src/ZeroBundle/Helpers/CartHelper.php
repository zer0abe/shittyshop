<?php

namespace ZeroBundle\Helper;

use ZeroBundle\Entity\Cart;
use ZeroBundle\Entity\Customer;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpFoundation\Session\Session;
use Doctrine\Bundle\DoctrineBundle\Registry as Doctrine;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class CartHelper implements ContainerAwareInterface
{
		use ContainerAwareTrait;

		private $doctrine;

		private $tokenStorage;

		public function __construct(Doctrine $doctrine, TokenStorage $storage){
			$this->doctrine = $doctrine;
			$this->tokenStorage = $storage;
		}
		public function cart(){
				$em = $this->doctrine->getManager();
				$session = new Session();
				if(!$this->tokenStorage->getToken()->isAuthenticated()) {
						if (!$session->has('cart') || empty($session->get('cart'))) {
								$cart = new Cart();
								$em->persist($cart);
								$em->flush();
								$session->set('cart', $cart->getId());
								return $cart;
						}else{
								$cart = $em->getRepository('ZeroBundle:Cart')->findOneBy(array('id' => $session->get('cart')));
								return $cart;
						}
				}else if(!$cart = $customer->getCart()){
						if($session->has('cart') && !empty($session->get('cart'))){
								$cart = $em->getRepository('ZeroBundle:Cart')->findOneBy(array('id' => $session->get('cart')));
								$customer->setCart($cart);
								return $cart;
						}
						return $cart;
				}
				return $cart;
		}
}
