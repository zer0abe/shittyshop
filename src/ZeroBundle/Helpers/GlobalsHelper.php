<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 16.02.16
 * Time: 21:13
 */

namespace ZeroBundle\Helpers;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Doctrine\ORM\EntityManager;

class GlobalsHelper {

		private $container;
		private $em;

		public function __construct(Container $container, EntityManager $em) {
				$this->container = $container;
				$this->em = $em;
		}

		public function getSuperCategories(){
			$super = $this->em->getRepository('ZeroBundle:SuperCategory')->findAll();
			return $super;
		}

		public function getCategories($sId = null){
			if(null == $sId) return $this->em->getRepository('ZeroBundle:Category')->findAll();
			return $this->em->getRepository('ZeroBundle:Category')->findBy(array('supercategory' => $sId));
		}

		public function getMinPrice(){
			return $this->em->getRepository('ZeroBundle:Product')->returnMinPrice();
		}

		public function getMaxPrice(){
			return $this->em->getRepository('ZeroBundle:Product')->returnMaxPrice();
		}

		public function getManufacturers(){
			return $this->em->getRepository('ZeroBundle:Manufacturer')->findAll();
		}

		public function copy($obj){
			$entityName = get_class($obj);
			$newObj = new $entityName();
			$vars = get_object_vars($obj);
			foreach($vars as $k => $v){
				if($k != 'id')
				$newObj->set.$k($v);
			}
			return $newObj;
		}

	/**
	 * Таблица перекодировки
	 * @var <array>
	 */
	static $transliteration_table = array(
			'а'=>'a', 'б'=>'b', 'в'=>'v', 'г'=>'g', 'д'=>'d', 'е'=>'e', 'ж'=>'g', 'з'=>'z',
			'и'=>'i', 'й'=>'y', 'к'=>'k', 'л'=>'l', 'м'=>'m', 'н'=>'n', 'о'=>'o', 'п'=>'p',
			'р'=>'r', 'с'=>'s', 'т'=>'t', 'у'=>'u', 'ф'=>'f', 'ы'=>'i', 'э'=>'e', 'А'=>'A',
			'Б'=>'B', 'В'=>'V', 'Г'=>'G', 'Д'=>'D', 'Е'=>'E', 'Ж'=>'G', 'З'=>'Z', 'И'=>'I',
			'Й'=>'Y', 'К'=>'K', 'Л'=>'L', 'М'=>'M', 'Н'=>'N', 'О'=>'O', 'П'=>'P', 'Р'=>'R',
			'С'=>'S', 'Т'=>'T', 'У'=>'U', 'Ф'=>'F', 'Ы'=>'I', 'Э'=>'E', 'ё'=>"yo", 'х'=>"h",
			'ц'=>"ts", 'ч'=>"ch", 'ш'=>"sh", 'щ'=>"shch", 'ъ'=>"", 'ь'=>"", 'ю'=>"yu", 'я'=>"ya",
			'Ё'=>"YO", 'Х'=>"H", 'Ц'=>"TS", 'Ч'=>"CH", 'Ш'=>"SH", 'Щ'=>"SHCH", 'Ъ'=>"", 'Ь'=>"",
			'Ю'=>"YU", 'Я'=>"YA", " "=>"_", "-" => "_",
	);

	/**
	 * Транслитерация строки
	 * @param $string
	 * @return string
	 */
	public static function transliterate($string)
	{
		return strtr( $string, self::$transliteration_table );
	}

	public function getProducts($catId = null){
		if(null == $catId) {
			return $this->em->getRepository('ZeroBundle:Product')->findAll();
		}else{
			return $this->em->getRepository('ZeroBundle:Product')->findBy(array('category' => $catId));
		}
	}
}