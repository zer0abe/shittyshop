if(jQuery('#isMainPage').data('main-page')) {
    var pager = new Pager(null, null, '#content');
    pager.get_elements_from_existing();
    var pagination = new Pagination(pager);
    var filter = new Filter(ajax, pagination, pager);

    var rangeSlider = jQuery('#slider').ionRangeSlider({
        min: filter.min_price,                        // минимальное значение
        max: filter.max_price,                       // максимальное значение
        //from: filter.min_price,                       // предустановленное значение ОТ
        //to: filter.max_price,                         // предустановленное значение ДО
        type: "double",                 // тип слайдера
        step: 1,                       // шаг слайдера
        postfix: " Р.",                  // постфикс значение
        hasGrid: true,                  // показать сетку
        gridMargin: 7,                  // отсуп от края сетки до края слайдера
        hideMinMax: true,               // спрятать поля Min и Max
        hideFromTo: true,               // спрятать поля From и To
        prettify: true,                 // разделять цифры пробелами 10 000
        disable: false,                 // заблокировать слайдер
        onLoad: function (obj) {        // callback, вызывается при запуске и обновлении
            console.log(obj);
        },
        onChange: function (obj) {      // callback, вызывается при каждом изменении состояния

        },
        onFinish: function (obj) {      // callback, вызывается один раз в конце использования
            filter.min_price = obj.from;
            filter.max_price = obj.to;
            filter.request();
            console.log(filter);
        }
    });

    jQuery('#like').on('keyup', function () {
        filter.set_search_string(this);
    });

    jQuery('#supercat_select').on('click', function () {
        var clicked = jQuery('#supercat_select').data('clicked');
        var menu = jQuery('#supercat_list');
        if (!clicked) {
            menu.slideDown();
            jQuery('#supercat_select').data('clicked', 1);
        }
        else {
            menu.slideUp();
            jQuery('#supercat_select').data('clicked', 0);
        }
    });

    jQuery('#cat_select').on('click', function(){
        var clicked = jQuery('#cat_select').data('clicked');
        console.log(clicked);
        var menu = jQuery('#cats-list');
        console.log(menu);
        if (!clicked) {
            console.log('Кликнул!');
            menu.slideDown();
            jQuery('#cat_select').data('clicked', 1);
            //document.getElementById('cat_select').setAttribute('data-clicked', '1');
        }
        else {
            console.log('Второй раз кликнул!');
            menu.slideUp();
            jQuery('#cat_select').data('clicked', 0);
            //document.getElementById('cat_select').setAttribute('data-clicked', '0');
        }
    });

    jQuery('.popup-item').each(function () {
        jQuery(this).on('click', function () {
            filter.get_cats(this);
            var html = this.innerHTML;
            jQuery('#supercat_select').html(html);
            jQuery('#cat_select').html(html);
            jQuery('#supercat_select').data('clicked', 0);
            jQuery(this).parent().hide();
        })
    });

    jQuery('.popup-cat-item').each(function () {
        jQuery(this).on('click', function () {
            filter.get_cats(this);
            var html = this.innerHTML;
            jQuery('#cat_select').html(html);
            jQuery('#cat_select').data('clicked', 0);
            jQuery(this).parent().hide();
        })
    });

    jQuery('.sort-by').on('click', function(){
        filter.set_order(this);
    });

    jQuery('.order-dir').on('click', function(){
        filter.set_order_dir(this);
    });

    pagination.set_vars();
    pagination.paginate();

    window.onresize = function () {
        pagination.set_vars();
        pager.render_page(pagination.start_element, pagination.stop_element);
        addToCartClick();
    };

    addToCartClick();
}
// Кусок кода для масштабирования текста внутри элемента и визуального эффекта при наведении мышиного курсора.
// Peace of crap...
// Maybe later...
//(function(jQuery) {
//    jQuery.fn.textfill = function(options) {
//        var fontSize = options.maxFontPixels;
//        var ourText = jQuery('span.std-item-price', this);
//        var maxHeight = jQuery(this).height();
//        var maxWidth = jQuery(this).width();
//        var textHeight;
//        var textWidth;
//        do {
//            ourText.css('font-size', fontSize);
//            textHeight = ourText.height();
//            textWidth = ourText.width();
//            fontSize = fontSize - 1;
//        } while ((textHeight > maxHeight || textWidth > maxWidth) && fontSize > 3);
//        return this;
//    }
//})(jQuery);
//
//jQuery(document).ready(function() {
//    jQuery('.std-item-to-cart').textfill({ maxFontPixels: 50 });
//});
//
//jQuery('.std-item-to-cart').each(function(){
//    jQuery(this).on('mouseover', function(){
//        var cart = jQuery(this).find('span:first');
//        var price = jQuery(this).find('span.std-item-price');
//        cart.animate({right: -50}, 500);
//        price.animate({left: 0,
//                        //fontSize: 14,
//                        top: -25}, 500);
//    });
//    jQuery(this).on('mouseout', function(){
//        var cart = jQuery(this).find('span:first');
//        var price = jQuery(this).find('span.std-item-price');
//        cart.animate({right: 0}, 500);
//        price.animate({left: -50}, 500);
//    })
//
//});

jQuery(function ($) {
    $('input[type=number]').iLightInputNumber();
});