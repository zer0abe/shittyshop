function Filter(ajax, paginator, pager, props){
    this.categories = [];
    this.manufacturers = [];
    this.chosen_cat = '';
    this.min_price = document.getElementById('min_p').value;
    this.max_price = document.getElementById('max_p').value;
    this.order = '';
    this.order_dir = '';
    this.limit = '';
    this.search_string = '';
    //this.first_run = true;
    this.form = $('#filter_form');
    this.ajax = ajax;
    this.answer = {};

    //Делаем запрос на сервер для получения отфильтрованных продуктов
    this.request = function(){
        var body = 'filter=filter';
        if(this.min_price) {
            var min = this.min_price * 1;
            body += '&min_p=' + min;
        }
        if(this.max_price){
            var max = this.max_price*1;
            body += '&max_p=' + max;
        }
        if(this.search_string){
            body += '&like='+this.search_string;
        }
        if(this.chosen_cat){
            body += '&category='+this.chosen_cat;
        }

        if(this.limit){
            body += '&limit' + this.limit*1;
        }

        body += '&order='+(this.order ? this.order : 'name');
        body += '&dir='+(this.order_dir ? this.order_dir : 'ASC');

        var self = this;
        console.log(body);

        var ajax = this.ajax;
        ajax.open('POST', '/_init');
        ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        ajax.onreadystatechange = function(){
            if(ajax.readyState != 4) return;
            if(ajax.status == 200){
                var response = JSON.parse(ajax.responseText);
                jQuery(pager.container).empty();
                for(var o in response){
                    if(response.hasOwnProperty(o) && o !== 'max_p' && o !== 'min_p') {
                        //console.log(o);
                        pager.get_dom_from_response(response[o]);
                    }
                    if(response.hasOwnProperty(o) && (o === 'max_p' || o === 'min_p')){
                        if(o === 'min_p') {
                            self.set_min_price(response[o]);
                        }else{
                            self.set_max_price(response[o]);
                        }
                    }
                }
                pager.get_elements_from_existing();
                pagination.set_vars();
                pager.render_page(pagination.start_element, pagination.stop_element);
            }
        };
        ajax.send(body);
    };

    this.get_cats = function(e){
        var self = this;
        var cat_id = jQuery(e).data('id');
        var ajax = this.ajax;
        var body = 'filter=get_cats&super_category='+cat_id;
        //console.log(body);
        ajax.open('POST', '/_init');
        ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        ajax.onreadystatechange = function() {
            if (ajax.readyState != 4) return;
            if (ajax.status == 200) {
                var response = JSON.parse(ajax.responseText);
                var select = jQuery('#cats-list');
                    select.empty();
                if(!response['nocategories']){
                    jQuery('#cats').removeClass('hidden');
                    jQuery('#cats-list').removeClass('hidden');
                    for(var o in response) {
                        if (response.hasOwnProperty(o)) {
                            //console.log(response[o]);
                            self.categories[o] = response[o];
                            var tmp = document.createElement('span');
                            jQuery(tmp).attr('class', 'popup-cat-item');
                            jQuery(tmp).attr('data-id', o);
                            jQuery(tmp).html(response[o]);
                            jQuery(tmp).on('click', function(){
                                self.set_category(this);
                                var html = this.innerHTML;
                                jQuery('#cat_select').html(html);
                                jQuery(this).parent().data('clicked', 0);
                                jQuery(this).parent().hide();
                            });
                            select.append(tmp);
                        }
                    }
                }else {
                    if(!jQuery('#cats').hasClass('hidden'))
                    jQuery('#cats').addClass('hidden');
                }

            }
        };
        ajax.send(body);

    };

    this.set_category = function(e){
        this.chosen_cat = jQuery(e).data('id');
        this.request();
    };

    this. set_min_price = function(e){
        jQuery('#min_p').val(e);
        this.min_price = e;
    };

    this.set_max_price = function(e){
        jQuery('#max_p').val(e);
        this.max_price = e;
    };

    this.set_order = function(e){
        this.order = $(e).data('sort');
        this.request();
    };

    this.set_order_dir = function(e){
        this.order_dir = $(e).data('order');
        this.request();
    };

    this.set_limit = function(e){
        var o = e.currentTarget;
        this.limit = $(o).val()*1;
    };

    this.set_search_string = function(e){
        this.search_string = $(e).val();
        this.request();
    };

}

function Pager(container_selector){
    this.elements = [];
    this.length = this.elements.length;
    this.rendered_elements = [];
    this.container = container_selector ? document.querySelector(container_selector) :
        document.getElementById('content');

    //this.filter = new Filter(ajax, null, this);

    //Создаём необходимый DOM элемент с указанными атрибутами
    this.returnElem = function(tag, attrs, children){
        var tmp = document.createElement(tag);
        if(attrs){
            if(attrs instanceof Object) {
                for (var attr in attrs) {
                    if (attrs.hasOwnProperty(attr))
                        if(attr == 'innerHTML')
                            tmp.innerHTML = attrs[attr];
                        else tmp.setAttribute(attr, attrs[attr]);
                }
            }else throw TypeError('Here must be an Object in second argument of returnElem function');
        }
        if(children){
            if(children instanceof Object){
                for(var prop in children){
                    if(children.hasOwnProperty(prop)){
                        var child = document.createElement(children[prop].tagName);
                        for (var a in children[prop].attrs) {
                            if (children[prop].attrs.hasOwnProperty(a)) {
                                if (attr == 'innerHTML')
                                        child.innerHTML = children[prop].attrs[a];
                                else child.setAttribute(a, children[prop].attrs[a]);
                            }
                            tmp.appendChild(child);
                        }
                    }
                }
            }else throw TypeError('Here must be an Object in third argument of returnElem function');
        }
        return tmp;
    };


    //Разбираем объект на DOM елементы
    this.parseObject = function(obj){
        if(obj instanceof Object){
            for(var prop in obj){
                if(obj.hasOwnProperty(prop)) {
                    //console.log('prop= '+prop);
                    if ((obj[prop] instanceof Object) && prop != 'attrs' && prop != 'children') {
                        //console.log('before parse prop= '+prop);
                        this.parseObject(obj[prop]);
                    } else {
                        if (prop == 'tagName') {
                            var tmp = this.returnElem(obj[prop], obj['attrs'], obj['children']);
                            this.addElement(tmp);
                        }
                    }
                }
            }
        }else throw TypeError('Here must be an Object in argument of parseObject function')
    };

    //Очищаем страницу
    this.clear = function () {
        var l = this.rendered_elements.length;
        if(l > 0){
            for(var i = 0; i<l-1; i++){
                //console.log(this.rendered_elements[i]);
                //this.container.removeChild(this.rendered_elements[i]);
                //delete this.rendered_elements[i];
            }
            $(this.container).empty();
        }
    };

    this.get_dom_from_response = function(o){
        //console.log(o + 'Before check');
        if(o instanceof Object){
            //console.log(o);
            jQuery(this.container).append('<div class="std-item '+ (o.buddyLike ? 'lookAtMe' : '') +' hidden">' +
                '<div class="std-item-img">' +
                '<a href="'+ o.productLink +'">' +
                '<img src="'+ o.imageSrc+'" alt=""/></a></div>' +
                '<p class="std-item-name">'+ o.name +'</p>' +
                '<div class="std-item-text">' +
                o.introText +
                '</div><a href="'+ o.productLink +'">' +
                '<button class="rect-btn" raised ripple-color="#9C27B0">'+
                '<i style="font-size: 18px; margin-right: 5px" class="fa fa-eye"></i>'+
                'Подробнее' +
                '<span class="std-item-price"><span class="rubbles">'+ o.calculatedPrice +'</span> за 1'+ o.unit +'</span>' +
                '</button></a>' +
                '<div class="std-item-to-cart" fab bg-pink600 z-1 data-pid="'+ o.id +'">' +
                '<i class="fa fa-cart-plus addtocart"></i>' +
                '</div>')
        }
    };

    //Рисуем страницу

    this.render_page = function(start, stop){
        var container = this.container;
        jQuery(container).css({opacity: 0});
        //jQuery(container).addClass()
        if(this.rendered_elements.length){
            var l = this.rendered_elements.length;
            for(var j = 0; j < l; j++){
                tmp = this.rendered_elements[j];
                //jQuery(tmp).animate({height: "hide"}, 1000);
                this.rendered_elements.splice(j, 1);
            }
        }
        $('.std-item').remove();
        var clr = this.returnElem('div', {'class': 'clear'});
        for(var i = start; i <= stop; i++){
            var tmp = this.elements[i];
            this.rendered_elements.push(tmp);
            //jQuery(tmp).removeClass('hidden');
            //jQuery(tmp).animate({height: "show"}, 1000);
            //$(tmp).css({'display': 'inline-block'});
            container.appendChild(tmp);
        }
        jQuery(container).animate({opacity: 1}, 1000);
        container.appendChild(clr);
        addToCartClick();
    };

    //Добавляем элемент в класс
    this.addElement = function(elem){
        this.elements.push(elem);
        this.length++;
    };

    //Пересчитываем элементы
    this.count_elements = function(){
        this.length = this.elements.length;
        return this.elements.length;
    };

    this.get_elements_from_existing =function(){
        this.elements.length = 0;
        var p = document.getElementsByClassName('std-item');
        var l = p.length;
        for(var i=0; i < l; i++){
            className = p[i].className;
            if(className.indexOf('hidden')+1){
                className = className.substr(0, className.indexOf('hidden')-1);
                p[i].className = className;
            }
            this.addElement(p[i]);
        }
        $(this.container).empty();
        $(this.container).append('<header><h1>Спешите приобрести!</h1></header>');
    };


}

function Pagination(pager){
    if(!(pager instanceof Pager))throw TypeError('Неверный аргумент!');

    this.start_element = 0;
    this.stop_element = 10;
    this.num_pages = 0;
    this.cur_page = 1;
    this.limit = 0;
    this.pagination_container = '';
    this.paginated = false;


    this.next_page = function(){
        if(this.cur_page == this.num_pages) return;
        this.cur_page++;
        this.set_vars();
        $('#page_num_'+(this.cur_page-1)).removeClass('current');
        $('#page_num_'+this.cur_page).addClass('current');
        pager.render_page(this.start_element, this.stop_element);
    };

    //Хотим на предыдущую
    this.prev_page = function(){
        if(this.cur_page < 2) return;
        this.cur_page--;
        this.set_vars();
        $('#page_num_'+(this.cur_page+1)).removeClass('current');
        $('#page_num_'+this.cur_page).addClass('current');
        pager.render_page(this.start_element, this.stop_element);
    };

    //Хотим вот на ЭТУ страницу
    this.get_page = function(page){
        if(page == this.cur_page) return;
        if(this.num_pages >= page && page >= 0) {
            this.cur_page = page;
            this.set_vars();
            pager.render_page(this.start_element, this.stop_element);
        }
    };

    this.paginate = function(){
        jQuery('.pagination_container').empty();
        this.pagination_container = this.pagination_container ?
            this.pagination_container : pager.returnElem('div', {'class': 'pagination_container'});
        this.set_vars();
        pager.render_page(this.start_element, this.stop_element);
        if(this.paginated){
            //return;
        }
        var pagination = this;
        var pagination_container = this.pagination_container;
        var i = 0;
        var l = this.num_pages;

        //Кнопка "Влево"
        var left = pager.returnElem('div', {
            'class': 'prev_page',
            'id': 'prev_page',
            'icon': 'chevron-left'
        });

        left.onclick = function(){
                    pagination.prev_page();
                };

        pagination_container.appendChild(left);
        while (i < l) {
            var s = this.countStart(i+1);
            //console.log('countStart '+this.countStart(i+1));
            var e = this.countStop(i+1);
            //console.log('countStop '+this.countStop(i+1));
            var page_num_div = pager.returnElem('div', {'class': 'page_num', 'id': 'page_num_'+(i+1),
                                                        'data-page-num': i+1, 'data-start': s, 'data-stop': e});
            page_num_div.innerHTML = (i + 1);
            page_num_div.onclick = function(){
                var current = $(this).data('pageNum');
                $('.current').removeClass('current');
                this.className += ' current';
                pagination.get_page(current);
            };
            if (this.cur_page == i+1)
                page_num_div.className += ' current';
            this.pagination_container.appendChild(page_num_div);
            i++;
        }

        //Кнопка "Вправо"
        var right = pager.returnElem('div', {
            'class': 'next_page',
            'id': 'next_page',
            'icon': 'chevron-right'
        });

        right.onclick = function(){
            pagination.next_page();
        };

        pagination_container.appendChild(right);

        document.body.appendChild(this.pagination_container);

        this.paginated = true;
    };

    this.set_vars = function(){
        var cw = pager.container.clientWidth-40;
        var ch = document.getElementById('drawer').clientHeight - 60;
        var ew = 180;
        var eh = 340;
        var max_rows = Math.floor(ch/eh);
        var max_cols = Math.floor(cw/ew);
        this.limit = max_cols * max_rows;
        var cur_num_pages = this.num_pages;
        this.num_pages = Math.ceil(pager.elements.length / this.limit);
        if(this.num_pages != cur_num_pages){
            this.paginate();
        }
        this.start_element = this.cur_page == 1 ? this.cur_page-1 : (this.cur_page-1)*this.limit;
        this.stop_element = this.limit-1 > (pager.elements.length - this.start_element-1) ?
            pager.elements.length - 1 :
            this.start_element + this.limit -1;
        //console.log('cw='+cw);
        //console.log('ch='+ch);
        //console.log('ew='+ew);
        //console.log('eh='+eh);
        //console.log('max-rows='+max_rows);
        //console.log('max-cols='+max_cols);
        //console.log('limit='+this.limit);
    };

    this.countStart = function(page){
        if(page > this.num_pages || page < 1) throw Error('Неправильное значение номера страницы');
        return page==1 ? page-1 : (page-1)*this.limit;
    };

    this.countStop = function(page){
        if(page > this.num_pages || page < 1) throw Error('Неправильное значение номера страницы');
        return ((page*this.limit + this.limit) > pager.elements.length) ?
            pager.elements.length-1 :
            (page-1)*this.limit + this.limit-1;
    }
}

