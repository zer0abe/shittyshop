jQuery(function(){

    var form_div = jQuery('#new_product_form_prototype');
    var s_cat_select = jQuery('#s_category');
    var cat_select = jQuery('#p_category');
    var man_select = jQuery('#m_id');
    var edit_s_cat_form = jQuery('#edit_s_cat_form');
    var new_s_cat_form = jQuery('#new_s_cat_form');
    var new_cat_form = jQuery('#new_cat_form');
    var edit_cat_form = jQuery('#edit_cat_form');
    var new_man_form = jQuery('#new_man_form');
    var edit_man_form = jQuery('#edit_man_form');
    var q_m_container = jQuery('#product_list_container');

    jQuery('#add_product').on('click', function(){
        if(form_div.hasClass('hidden')){
            form_div.show();
        }
    });

    var find_selectors = function(form){
        var selectors = [];
        jQuery(form).find('[data-selector]').each(function(){
                if(selector = jQuery(this).data('selector')){
                    selectors.push(selector);
                }
        });
        return selectors;
    };


    var save = function(e){
        e.preventDefault();
        var $that = jQuery(e.currentTarget);
        var formdata = new FormData($that.get(0));
        var k, v;
        console.log(formdata);
        jQuery.ajax({
            url: $that.attr('action'),
            type: 'POST',
            data: formdata,
            dataType: 'json',
            success: function(json){
                console.log(json);
                k = json.id;
                v = json.name;
            }
        });
        return [k, v];
    };

    jQuery('.close_form').on('click', function(){
            jQuery(this).parent().hide();
    });

    jQuery('#add_s_category').on('click', function(){
        var form = jQuery('#edit_s_cat_form');
        form.attr('card', '');
        form.attr('z-3', '');
        form.addClass('appended_form');
        form.show();
    });

    jQuery('#add_category').on('click', function(){
        var form = new_cat_form;
        form.attr('card', '');
        form.attr('z-3', '');
        form.addClass('appended_form');
        form.show();
    });


    jQuery('#add_category').on('click', function(){
        //var current_val = jQuery('#p_category').val();

        var form = new_cat_form;
        form.attr('card', '');
        form.attr('z-3', '');
        form.addClass('appended_form');
        form.find('#save_cat').on('click', function(e){
                    e.preventDefault();
                    var selectors = find_selectors(form);
                    console.log(selectors);
                    var formdata = new FormData(document.forms.new_cat_form);
                    console.log('formdata: '+formdata);
                    formdata.append('trigger', 'category');
                    formdata.append('action', 'new');
                    jQuery.ajax({
                        url: '/admin/_ajax_choice',
                        type: 'POST',
                        contentType: false,
                        processData: false,
                        data: formdata,
                        dataType: 'json',
                        success: function(r){
                            cat_select.append('<option value="' + r.id + '" id="cat_'+ r.id +'">' + r.name + '</option>');
                            old_html = form.find('#save_cat').html();
                            form.find('#save_cat').text('Успешно!');
                            setTimeout(function(){
                                form.find('.close_form').trigger('click');
                                form.find('#save_cat').html(old_html);
                            }, 2000);
                            //console.log(r);
                        }
                    })
        })
        form.show();
    });

    jQuery('#edit_category').on('click', function(){
        var current_val = jQuery('#p_category').val();

        var form = edit_cat_form;
        form.attr('card', '');
        form.attr('z-3', '');
        form.addClass('appended_form');
        jQuery.ajax({
            url: '/admin/_ajax_choice',
            type: 'POST',
            //contentType: false,
            //processData: false,
            data: {
                'trigger': 'category',
                'action': 'get',
                'cat_id': current_val
            },
            dataType: 'json',
            success: function(json){
                console.log(json);
                var selectors = [];
                for(var o in json){
                    if(json.hasOwnProperty(o)) {
                        form.find('#cat_' + o).val(json[o]);
                        selectors.push('#cat_' + o);
                    }

                }
                form.find('#save_cat').on('click', function(e){
                    e.preventDefault();
                    var formdata = new FormData(document.forms.edit_cat_form);
                    formdata.append('trigger', 'category');
                    formdata.append('action', 'edit');
                    formdata.append('cat_id', current_val);
                    console.log(formdata);
                    jQuery.ajax({
                        url: '/admin/_ajax_choice',
                        type: 'POST',
                        data: formdata,
                        contentType: false,
                        processData: false,
                        dataType: 'json',
                        success: function(r){
                            cat_select.find('#cat_'+r.id).attr('id', r.id).html(r.name);
                            form.find('#save_cat').text('Успешно!');
                            setTimeout(function(){
                                form.find('.close_form').trigger('click');
                            }, 2000);
                        }
                    })
                })
            }
        });
        form.show();
    });

    jQuery('#edit_s_cat').on('click', function(){
        var current_val = jQuery('#s_category').val();

        var form = edit_s_cat_form;
        form.attr('card', '');
        form.attr('z-3', '');
        form.addClass('appended_form');

        jQuery.ajax({
            url: '/admin/_ajax_choice',
            type: 'POST',
            //contentType: false,
            //processData: false,
            data: {
                'trigger': 'super_cat',
                'action': 'get',
                's_id': current_val
            },
            dataType: 'json',
            success: function(json){
                console.log(json);
                var selectors = [];
                for(var o in json){
                    if(json.hasOwnProperty(o)) {
                        form.find('#' + o).val(json[o]);
                        selectors.push('#' + o);
                    }
                }
                form.find('.save_s_cat').on('click', function(e){
                    e.preventDefault();
                    var data = {trigger:'super_cat',
                                action: 'edit',
                                s_id: current_val};
                    for(var i=0; i<selectors.length; i++){
                        val = form.find(selectors[i]).val();
                        data[selectors[i].substr(1)] = val;
                    }

                    jQuery.ajax({
                        url: '/admin/_ajax_choice',
                        type: 'POST',
                        data: data,
                        dataType: 'json',
                        success: function(r){
                            s_cat_select.find('#scat_'+r.id).attr('id', r.id).html(r.name);
                            old_html = form.find('.save_s_cat').html();
                            form.find('.save_s_cat').text('Успешно!');
                            setTimeout(function(){
                                form.find('.close_form').trigger('click');
                                form.find('.save_s_cat').html(old_html);
                            }, 2000);
                        }
                    })
                })
            }
        });
        form.show();
    });

    jQuery('#remove_s_category').on('click', function(){
        var select = jQuery('#s_category');
        var current_val = select.val();

        jQuery.ajax({
            url: '/admin/_ajax_choice',
            type: 'POST',
            //contentType: false,
            //processData: false,
            data: {
                'trigger': 'super_cat',
                'action': 'delete',
                's_catId': current_val
            },
            dataType: 'json',
            success: function(json){
                console.log(json);

                for(var o in json){
                    if(json.hasOwnProperty(o))
                        select.append('<option value="' + o + '">' + response[o] + '</option>');                }
            }
        });
        form.show();
    });

    jQuery('#add_manuf').on('click', function(){
        var form = new_man_form;
        form.attr('card', '');
        form.attr('z-3', '');
        form.addClass('appended_form');
        form.find('#save_manuf').on('click', function(e){
            e.preventDefault();
            var formdata = new FormData(document.forms.new_manufacturer);
            formdata.append('trigger', 'manufacturer');
            formdata.append('action', 'new');
            console.log(formdata);
            jQuery.ajax({
                url: '/admin/_ajax_choice',
                type: 'POST',
                data: formdata,
                contentType: false,
                processData: false,
                dataType: 'json',
                success: function(r){
                    man_select.append('<option value="' + r.id + '" id="m_'+ r.id +'">' + r.name + '</option>');
                    var old_html = form.find('#save_manuf').html();
                    form.find('#save_manuf').text('Успешно!');
                    setTimeout(function(){
                        form.find('.close_form').trigger('click');
                        form.find('#save_manuf').html(old_html)
                    }, 2000);
                }
            })
        });
        form.show();
    });

    jQuery('#edit_manuf').on('click', function(){
        var current_val = jQuery('#m_id').val();

        var form = edit_man_form;
        form.attr('card', '');
        form.attr('z-3', '');
        form.addClass('appended_form');
        jQuery.ajax({
            url: '/admin/_ajax_choice',
            type: 'POST',
            //contentType: false,
            //processData: false,
            data: {
                'trigger': 'manufacturer',
                'action': 'get',
                'm_id': current_val
            },
            dataType: 'json',
            success: function(json){
                console.log(json);
                for(var o in json){
                    if(json.hasOwnProperty(o)) {
                        form.find('#m_' + o).val(json[o]);
                    }

                }
                form.find('#save_manuf').on('click', function(e){
                    e.preventDefault();
                    var formdata = new FormData(document.forms.edit_manufacturer);
                    formdata.append('trigger', 'manufacturer');
                    formdata.append('action', 'edit');
                    formdata.append('m_id', current_val);
                    console.log(formdata);
                    jQuery.ajax({
                        url: '/admin/_ajax_choice',
                        type: 'POST',
                        data: formdata,
                        contentType: false,
                        processData: false,
                        dataType: 'json',
                        success: function(r){
                            cat_select.find('#m_'+r.id).attr('id', r.id).html(r.name);
                            form.find('#save_cat').text('Успешно!');
                            setTimeout(function(){
                                form.find('.close_form').trigger('click');
                            }, 2000);
                        }
                    })
                })
            }
        });
        form.show();
    });

    jQuery('#save_product').on('click', function(e){
        e.preventDefault();
        var form = form_div;
        var formdata = new FormData(document.forms.new_product);
        formdata.append('trigger', 'product');
        formdata.append('action', 'new');
        jQuery.ajax({
            url: '/admin/_ajax_choice',
            type: 'POST',
            data: formdata,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(r){
                var old_html = form.find('#save_product').html();
                for(o in r){
                    if(r.hasOwnProperty(o)){
                        form.find('#p_'+o).val('');
                    }
                }
                form.find('#save_product').text('Успешно!');
                setTimeout(function(){
                    form.find('.close_form').trigger('click');
                    form.find('#save_product').html(old_html);
                }, 2000);
            }
        })
    });

    s_cat_select.on('change', function(e){
        var cat_id = e.currentTarget.value;
        var ajax = getXmlHttp();
        var body = 'filter=get_cats&super_category='+cat_id;
        console.log(body);
        ajax.open('POST', '/_init');
        ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        ajax.onreadystatechange = function() {
            if (ajax.readyState != 4) return;
            if (ajax.status == 200) {
                var response = JSON.parse(ajax.responseText);
                var select = jQuery('#p_category');
                console.log(response);
                select.empty();
                if(!response['nocategories']){
                    select.removeClass('hidden');
                    jQuery('#categories_list').removeClass('hidden');
                    select.append('<option value ="">Выбери подкатегорию...</option>');
                    for(var o in response) {
                        if (response.hasOwnProperty(o)) {
                            console.log(response[o]);
                            select.append('<option value="' + o + '" id="cat_'+ o +'">' + response[o] + '</option>');
                        }
                    }
                }else {
                    if(!select.hasClass('hidden') && !jQuery('#categories_list').hasClass('hidden')) {
                        select.addClass('hidden');
                        jQuery('#categories_list').addClass('hidden')
                    }
                }
            }
        };
        ajax.send(body);
    });

    jQuery('#swap_product_list').on('click', function(){
        if(jQuery('#product_list_table').hasClass('hidden')){
            jQuery('#product_list_table').removeClass('hidden')
        }else{
            jQuery('#product_list_table').addClass('hidden')
        }
    });
    function fillForm(o, selector, prefix){
        var form = jQuery(selector);
        for(var obj in o){
            if(o.hasOwnProperty(obj)){
                form.find('[name='+prefix+obj+']').val(o[obj]);
                console.log(form.find('[name='+prefix+obj+']'))
            }
        }
    }
    jQuery('.edit_product').each(function(){
        jQuery(this).on('click', function(){
            var pid = jQuery(this).data('id');
                jQuery.ajax({
                url: '/admin/_ajax_choice',
                type: 'POST',
                data: {trigger: 'product',
                        action: 'get',
                        p_id: pid},
                //contentType: false,
                //processData: false,
                dataType: 'json',
                success: function(r){
                    console.log(r);
                    fillForm(r, '#edit_product_form', 'p_');
                    jQuery('#edit_product_form').show();
                }
            })
        })
    })
});